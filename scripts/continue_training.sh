#!/bin/bash
#SBATCH --nodes=1
#SBATCH --gres=gpu:v100l:4
#SBATCH --cpus-per-task=24
#SBATCH --mem=0
#SBATCH --account=def-stanmat-ab
#SBATCH --time=4-23:59
#SBATCH --output=logs/train-cont-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

PREV_LOG_FILE=$1

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate

python -u mmlnet/train_maskrcnn.py \
       -resume \
       --prev-log-file $PREV_LOG_FILE \
       --num-workers 24
