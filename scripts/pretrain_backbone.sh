#!/bin/bash
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=16
#SBATCH --mem=32000M
#SBATCH --account=def-stanmat-ab
#SBATCH --time=0-23:59
#SBATCH --output=logs/pretrain-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate

ARCH="resnet101"
SECONDS=45
CHANNELS=1
LEARNING_RATE=0.001
BATCH_SIZE=16

python -u mmlnet/pretrain_backbone.py \
       --run-id "stantec_${CHANNELS}x${SECONDS}" \
       --architecture $ARCH \
       --seconds $SECONDS \
       --learning-rate $LEARNING_RATE \
       --batch-size $BATCH_SIZE \
       --print-freq 1000
       
