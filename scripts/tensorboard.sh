#!/bin/bash
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=32000M
#SBATCH --account=def-stanmat-ab
#SBATCH --time=0-07:59
#SBATCH --output=logs/tensorboard-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate
tensorboard --logdir ./logs/tensorboard --host 0.0.0.0 --port 6006
