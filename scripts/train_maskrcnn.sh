#!/usr/bin/bash

python -u mmlnet/train_maskrcnn.py \
       --run-id "mammalnet_10s_500Hz" \
       --seconds 10 \
       --fmax 500 \
       --learning-rate 0.001 \
       --num-workers 4 \
       --batch-size 4 \
       --print-freq 250
