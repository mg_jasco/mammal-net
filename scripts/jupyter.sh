#!/bin/bash
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000M
#SBATCH --account=def-stanmat-ab
#SBATCH --time=0-23:59
#SBATCH --output=logs/jupyter-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate
bash $VIRTUAL_ENV/bin/jupyter-lab.sh
