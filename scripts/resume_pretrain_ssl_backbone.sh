#!/bin/bash
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=16
#SBATCH --mem=32000M
#SBATCH --account=def-stanmat-ab
#SBATCH --time=0-23:59
#SBATCH --output=logs/pretrain-ssl-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate

ARCH="resnet101"
SECONDS=45
CHANNELS=1
LEARNING_RATE=0.001
BATCH_SIZE=16

NOISE=1
TIME_WARPING=0
TIME_MASKING=50
FREQ_MASKING=50
N_TIME_MASKS=1
N_FREQ_MASKS=1

python -u mmlnet/pretrain_ssl_backbone.py \
       --run-id "minkenet101_SSL_${CHANNELS}x${SECONDS}_masks_${N_TIME_MASKS}x${N_FREQ_MASKS}_time_warping_${TIME_WARPING}_noise_${NOISE}_RESUMED" \
       -resume --previous-id "minkenet101_SSL_${CHANNELS}x${SECONDS}_masks_${N_TIME_MASKS}x${N_FREQ_MASKS}_time_warping_${TIME_WARPING}_noise_${NOISE}" \
       --architecture $ARCH -frozen-bn \
       --seconds $SECONDS \
       --channels $CHANNELS \
       --learning-rate $LEARNING_RATE \
       --batch-size $BATCH_SIZE \
       --time-warping $TIME_WARPING \
       --time-masking $TIME_MASKING \
       --freq-masking $FREQ_MASKING \
       --n-freq-masks $N_FREQ_MASKS \
       --n-time-masks $N_TIME_MASKS \
       --add-noise $NOISE \
       --print-freq 1000
