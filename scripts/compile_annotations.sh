#!/bin/bash
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=32000M
#SBATCH --account=def-stanmat-ab
#SBATCH --time=0-02:59
#SBATCH --output=logs/compile-annotations-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

module load gcc/7.3.0 r/3.6.0
Rscript data/compile_annotations.R --recompile
module purge

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate
python data/create_file_dicts.py
