#!/bin/bash
#SBATCH --nodes=1
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --account=def-stanmat-ab
#SBATCH --time=2-23:59
#SBATCH --output=logs/save-data-%N-%j.out
#SBATCH --mail-user=<markthomas@dal.ca>
#SBATCH --mail-type=ALL

module load nixpkgs/16.09 intel/2018.3 cuda/10.0 cudnn/7.4 python/3.6
source /home/mdjt/pytorch/bin/activate
python mmlnet/utils/save_hdf5_data.py
