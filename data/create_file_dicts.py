# create_file_dicts.py
#
# author: Mark Thomas
# modified: 2020-03-10

import os
import re
import pickle
import numpy as np
import pandas as pd

from glob import glob
from tqdm import tqdm


def main():
    files = glob('./data/splits/**/*.csv', recursive=True)

    with tqdm(total=len(files)) as pbar:
        for file in files:
            # Read the CSV and cast to a dictionary
            df = pd.read_csv(file).replace(np.nan, '', regex=True)
            file_dict = dict(zip(df.identifier, df.drop("identifier", axis=1).to_dict('records')))

            # Save the file dict using pickle
            file_dict_name = re.sub('(.*).csv', '\\1', file)
            with open('{}.pickle'.format(file_dict_name), 'wb') as f:
                pickle.dump(file_dict, f)

            # Remove the CSV file and update the progress bar
            os.remove(file)
            pbar.update(1)


if __name__ == '__main__':
    main()
