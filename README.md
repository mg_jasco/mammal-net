Mammal-Net v3
-------------
Official repository for JASCO Applied Sciences marine mammal species classification and detection development using deep learning.

The current version of Mammal-Net (v3) roughly follows that detailed in [1] (i.e., using Mask R-CNN to learn bounding boxes around the vocalizations of Blue, Fin, Humpback, Sei, and North Atlantic right whales within spectrograms.)

The previous version (v2) detailed in [2] has been deprecated in favour of v3.

### Requirements
Python dependencies

+ Python 3.6.3
+ numpy, pickle, librosa, soundfile, tqdm, argparse, pillow, pycocotools, jupyter\*, matplotlib\*
+ torch (*tested using 1.4.0*)
+ torchvision (*tested using 0.5.0*)

R dependencies (*for building the dataset splits*)

+ R 3.5.1
+ tidyverse (dplyr, readr)
+ lubridate
+ optparse
+ dplR (*for generating the unique annotation IDs*)

\* = *for visualization purposes only*

### Instructions
#### To build the datasets
Re-compiling the metadata is optional and the following step can be made a lot faster by copying the `annotation_metadata.csv` file from `\\jso-dmfs02\General\Mark\mammal-net-data` to the `data/metadata` directory.

If you do wish to recompile the entire dataset, copy the `annotations.csv` file from `\\jso-dmfs02a` to the `data/` directory and use the `--recompile` in the following script. __Disclaimer__: Re-compiling the datasets using the `--recompile` flag above will likely take several hours!

At the command line, run the following to generate the training, validation, and test splits:
```
$ Rscript data/compile_annotations.R --base-directory <YOUR BASE DIRECTORY>
```

To convert the CSVs to dictionaries and save them using pickle, run:
```
$ python data/create_file_dicts.py
```

#### To build Mammal-Net from source
__TODO__: finish this section after the code has all been moved over

Run:
```
$ pip install -r requirements.txt
```
followed by
```
$ python setup.py install
```

#### To test the model
__TODO__: finish this section


#### To train the model
Training is most suitable using NVIDIA GPUs on Compute Canada (SLURM).

To train the model from scratch, change the arguments found in `scripts/train_maskrcnn.sh` as desired and submit the job to SLURM via:
```
$ sbatch scripts/train_maskrcnn.sh
```


### In development
#### Semi-supervised learning
   - Work is ongoing for conducting unsupervised pre-training on the backbone network of Mask R-CNN using Invertible ResNets [3]. This work is found under the subdirectory `py/lib/inverible-resnet`.
   - Work is ongoing using a variant of Mixmatch [4] for object detection for partially labeled data. This work is currently not included in this repo.


### Maintainer
Mark Thomas ([mark.thomas@jasco.com](mailto:mark.thomas@jasco.com))

### References:
[1]  [Thomas et al. (2019). "Detecting Endangered Baleen Whales within Acoustic Recordings using Region-based Convolutional Neural Networks."](https://aiforsocialgood.github.io/neurips2019/accepted/track1/pdfs/2_aisg_neurips2019.pdf)

[2]  [Thomas et al. (2019). "Marine mammal species classification using convolutional neural networks and a novel acoustic representation."](https://arxiv.org/abs/1907.13188)

[3]  [Behrmann et al. (2019) "Invertible Residual Networks"](http://proceedings.mlr.press/v97/behrmann19a.html)

[4]  [Berthelot et al. (2019) "Mixmatch: A holistic approach to semi-supervised learning"](http://papers.nips.cc/paper/8749-mixmatch-a-holistic-approach-to-semi-supervised-learning)
