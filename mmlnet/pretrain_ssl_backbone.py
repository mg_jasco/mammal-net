# pretrain_ssl_backbone.py
#
# @author: Mark Thomas
# @modified: 2020-05-12

import os
import copy
import argparse
import random
import torch
import torchvision
import numpy as np
import multiprocessing as mp

from torch.utils.tensorboard import SummaryWriter
from utils.ssl_utils import create_model, train, validate, save_checkpoint, SemiLoss, WeightEMA, training_print_statement

from torch.utils.data import DataLoader
from utils.data_utils import HDF5Dataset
from utils.spec_augment import AddGaussianNoise, SpecAugment, TransformTwice
from utils.sampler import BalancedBatchSampler

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


parser = argparse.ArgumentParser(description='PyTorch MixMatch Training')

# Training params
parser.add_argument('--run-id', default='', type=str)
parser.add_argument('-resume', action='store_true', default=False)
parser.add_argument('--previous-id', default='', type=str)
parser.add_argument('--architecture', default='resnet101', type=str)
parser.add_argument('-frozen-bn', action='store_true', default=False)
parser.add_argument('--epochs', default=100, type=int)
parser.add_argument('--batch-size', default=128, type=int)
parser.add_argument('--learning-rate', default=0.001, type=float)
parser.add_argument('--decay-rate', default=0.1, type=float)
parser.add_argument('--print-freq', default=100, type=int)

# Data and SpecAugment params
parser.add_argument('--channels', default=1, type=int)
parser.add_argument('--seconds', default=30, type=int)
parser.add_argument('--add-noise', default=0, type=int)
parser.add_argument('--time-warping', default=50, type=int)
parser.add_argument('--time-masking', default=50, type=int)
parser.add_argument('--freq-masking', default=50, type=int)
parser.add_argument('--n-freq-masks', default=2, type=int)
parser.add_argument('--n-time-masks', default=2, type=int)

# MixMatch params
parser.add_argument('--alpha', default=0.75, type=float)
parser.add_argument('--lambda-u', default=75, type=float)
parser.add_argument('--T', default=0.5, type=float)
parser.add_argument('--ema-decay', default=0.999, type=float)

best_f1 = 0


def main():
    global best_f1
    args = parser.parse_args()
    
    # Print out the training statement and create the SummaryWriter
    training_print_statement(args)
    writer = SummaryWriter('{}/logs/tensorboard/{}'.format(HOME_DIR, args.run_id))
    
    print('--> Preparing the datasets')
    args.species_lookup = {'AB': 0, 'HB': 1, 'FW': 2, 'MW': 3}
    
    # Set up the SpecAugment transformation
    spec_augment = SpecAugment(time_warping=args.time_warping, time_masking=args.time_masking, n_time_masks=args.n_time_masks,
                               freq_masking=args.freq_masking, n_freq_masks=args.n_freq_masks)
   
    if args.add_noise:
        gaussian_noise = AddGaussianNoise()
        transform = torchvision.transforms.Compose([gaussian_noise, spec_augment])
    else:
        transform = torchvision.transforms.Compose([spec_augment])

    # Create the training datasets
    labeled_set = HDF5Dataset('{}/hdf5'.format(DATA_DIR), seconds=args.seconds, 
                              channels=args.channels, species_lookup=args.species_lookup,
                              return_ambient=True, shuffle=True, pretraining=True, seed=1234)
    
    unlabeled_set = HDF5Dataset('{}/hdf5'.format(DATA_DIR), seconds=args.seconds, 
                                channels=args.channels, species_lookup=None, pretraining=True, 
                                shuffle=True, seed=1234, transform=TransformTwice(transform))
    
    # Create the training loaders
    labeled_sampler = BalancedBatchSampler(labeled_set, list(args.species_lookup.values()))
    labeled_loader = DataLoader(labeled_set, batch_size=args.batch_size, sampler=labeled_sampler,
                                num_workers=mp.cpu_count(), pin_memory=True, drop_last=True)

    unlabeled_loader = DataLoader(unlabeled_set, batch_size=args.batch_size, shuffle=True,
                                  num_workers=mp.cpu_count(), pin_memory=True, drop_last=True)
    
    # Make a validation set/loader using a copy of the training set and changed to evaluation
    validation_set = copy.deepcopy(labeled_set)
    validation_set.eval()
    validation_loader = DataLoader(validation_set, batch_size=args.batch_size, 
                                   num_workers=mp.cpu_count(), drop_last=True)
    
    # Create the model
    model = create_model(args, ema=False)
    ema_model = create_model(args, ema=True)
    torch.backends.cudnn.benchmark = True
    print('--> Created the model (total params = {:.2f}M)'.format(sum(p.numel() for p in model.parameters()) / 1000000.0))

    # Create the training criterions, optimizers, and LR scheduler
    training_criterion = SemiLoss()
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    ema_optimizer = WeightEMA(model, ema_model, alpha=args.ema_decay, lr=args.learning_rate)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=args.decay_rate, patience=10)
    
    # Start the training
    args.n_labeled = len(labeled_loader)
    step = 0
    
    # Load the model/optimizer from a previous checkpoint
    if args.resume:
        checkpoint = torch.load('{}/results/{}/checkpoint.pth.tar'.format(HOME_DIR, args.previous_id))
        start_epoch = checkpoint['epoch'] + 1
        model.load_state_dict(checkpoint['state_dict'])
        ema_model.load_state_dict(checkpoint['ema_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
    else:
        start_epoch = 0

    print('--> Starting the training...')
    for epoch in range(start_epoch, start_epoch + args.epochs):
        print('\nEpoch [{: >3} / {}]'.format(epoch + 1, args.epochs))
        step = args.n_labeled * (epoch + 1)
        
        # Train the model for a full epoch
        loss, loss_x, loss_u = train(labeled_loader, unlabeled_loader, 
                                     model, optimizer, ema_optimizer, training_criterion, 
                                     epoch, use_cuda=True, args=args)
        
        # Run the evaluation on the training set
        _, acc, pre, rec = validate(labeled_loader, ema_model, criterion, epoch, use_cuda=True, args=args)
        print('| training set:  accuracy = {:.5f} | precision = {:.5f} | recall = {:.5f}'.format(acc, pre, rec))
        writer.add_scalar('losses/loss/training', loss, step)
        writer.add_scalar('losses/loss_X/training', loss_x, step)
        writer.add_scalar('losses/loss_U/training', loss_u, step)
        writer.add_scalar('metrics/accuracy/training', acc, step)
        writer.add_scalar('metrics/precision/training', pre, step)
        writer.add_scalar('metrics/recall/training', rec, step)

        # Run the evaluation on the validation set
        loss, acc, pre, rec = validate(validation_loader, ema_model, criterion, epoch, use_cuda=True, args=args)
        print('| validation set:  accuracy = {:.5f} | precision = {:.5f} | recall = {:.5f}'.format(acc, pre, rec))
        writer.add_scalar('losses/loss/validation', loss, step)
        writer.add_scalar('metrics/accuracy/validation', acc, step)
        writer.add_scalar('metrics/precision/validation', pre, step)
        writer.add_scalar('metrics/recall/validation', rec, step)
        
        # Check the LR scheduler
        lr_scheduler.step(loss)
        
        # Save the model
        f1 = (2 * pre * rec) / (pre + rec)
        if f1 > best_f1:
            best_f1 = f1
            is_best = True
        else:
            is_best = False
        
        save_checkpoint({
            'epoch': epoch,
            'state_dict': model.state_dict(),
            'ema_state_dict': ema_model.state_dict(),
            'optimizer' : optimizer.state_dict()
        }, is_best, checkpoint='./results/{}'.format(args.run_id))
          
    writer.close()
    

if __name__ == '__main__':
    main()
