from torch import nn
from torchvision.ops.feature_pyramid_network import FeaturePyramidNetwork, LastLevelMaxPool

from torchvision.ops import misc as misc_nn_ops
from torchvision.models._utils import IntermediateLayerGetter
import models


class BackboneWithFPN(nn.Module):
    """
    Adds a FPN on top of a model.
    Internally, it uses torchvision.models._utils.IntermediateLayerGetter to
    extract a submodel that returns the feature maps specified in return_layers.
    The same limitations of IntermediatLayerGetter apply here.
    Arguments:
        backbone (nn.Module)
        return_layers (Dict[name, new_name]): a dict containing the names
            of the modules for which the activations will be returned as
            the key of the dict, and the value of the dict is the name
            of the returned activation (which the user can specify).
        in_channels_list (List[int]): number of channels for each feature map
            that is returned, in the order they are present in the OrderedDict
        out_channels (int): number of channels in the FPN.
    Attributes:
        out_channels (int): the number of channels in the FPN
    """
    def __init__(self, backbone, return_layers, out_channels, in_channels_list=None, use_fpn=False):
        super(BackboneWithFPN, self).__init__()
        self.body = IntermediateLayerGetter(backbone, return_layers=return_layers)
        self.use_fpn = use_fpn
        if self.use_fpn:
            self.fpn = FeaturePyramidNetwork(
                in_channels_list=in_channels_list,
                out_channels=out_channels,
                extra_blocks=LastLevelMaxPool(),
            )
        self.out_channels = out_channels

    def forward(self, x):
        x = self.body(x)
        if self.use_fpn:
            x = self.fpn(x)
        return x


def cnn_fpn_backbone(args):
        
    architecture = args.backbone_architecture
    if not (architecture.startswith('res') or architecture.startswith('mobile')):
        raise ValueError("Currently only ResNet and MobileNet backbones are supported")
    
    if args.frozen_bn:
        norm_layer = misc_nn_ops.FrozenBatchNorm2d
    else:
        norm_layer = None
        
    backbone = models.__dict__[architecture](channels=args.channels, num_classes=args.backbone_classes, 
                                             pretrained=False, norm_layer=norm_layer)
    
    if args.backbone_checkpoint is not None:
        try:
            backbone = nn.DataParallel(backbone)
            backbone.load_state_dict(args.backbone_checkpoint['state_dict'])
            backbone = backbone.module
        except:
            try:
                backbone.load_state_dict(args.backbone_checkpoint['state_dict'])
            except:
                raise ValueError("Tried loading the backbone with/without DataParallel and failed to do so")
    
    # ResNet 
    if architecture.startswith('res'):
        if args.pretrained_backbone:
            for name, parameter in backbone.named_parameters():
                parameter.requires_grad_(False)

        return_layers = {'layer1': '0', 'layer2': '1', 'layer3': '2', 'layer4': '3'}    
        in_channels_stage2 = backbone.inplanes // 8    
        in_channels_list = [
            in_channels_stage2,
            in_channels_stage2 * 2,
            in_channels_stage2 * 4,
            in_channels_stage2 * 8,
        ]
        out_channels = 256
        include_fpn = True
        
    # MobileNet
    if architecture.startswith('mobile'):
        if args.pretrained_backbone:
            for name, parameter in backbone.named_parameters():
                parameter.requires_grad_(False)
                
        return_layers = {'features': '0'}
        in_channels_list = None
        out_channels = 1280
        include_fpn = False

    return BackboneWithFPN(backbone, return_layers, out_channels, in_channels_list, use_fpn=include_fpn)
