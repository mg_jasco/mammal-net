# pretrain_backbone.py
#
# @author: Mark Thomas
# @modified: 2020-07-22

import copy
import argparse
import random
import warnings
import torch
import torchvision
import numpy as np
import models

from torch.utils.tensorboard import SummaryWriter
from utils.data_utils import SpeciesDataset, HDF5Dataset
from utils.pretraining_utils import train, validate, test_best_metrics, save_checkpoint, read_log_file, training_print_statement
from utils.sampler import BalancedBatchSampler

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


# Set the global best metrics and recieve the available models
best_metrics = (0.0, 0.0, 0.0)

# Setup the ArgumentParser
parser = argparse.ArgumentParser(description='MammalNet v3 Backbone Pre-training')

# Data arguments
parser.add_argument('--seconds', default=15, type=int,
                    help='The number of seconds of each spectrogram')
parser.add_argument('--fmax', default=1000, type=int,
                    help='The maximum frequency of each spectrogram in Hz')
parser.add_argument('-fg-bg-sep', action='store_true', default=False,
                    help='Whether to seperate the foreground and background')
parser.add_argument('-mel', action='store_true', default=False,
                    help='Whether to convert the spectrogram to a mel-scale')

# Training arguments
parser.add_argument('--run-id', type=str,
                    help='The run ID and subsequent directory of the model results')
parser.add_argument('--split', default=1, type=int, choices=list(range(1, 20)),
                    help='The split number to use')
parser.add_argument('--split-type', default='partially_annotated/random_sample', type=str,
                    choices=['fully_annotated', 'partially_annotated/random_sample',
                             'partially_annotated/datetime_based'],
                    help='The type of data split to use (as in the data/splits subdirectory)')
parser.add_argument('--seed', default=None, type=int, help='The RNG seed')
parser.add_argument('--print-freq', default=100, type=int,
                    help='How often to print out training results')
parser.add_argument('--num-workers', default=24, type=int,
                    help='The number of cores or threads available to the data processor')

# Learning arguments
parser.add_argument('--architecture', default='resnet101', type=str,
                    help='The name of the CNN architecture to use')
parser.add_argument('-frozen-bn', action='store_true', default=False,
                    help='Whether to use frozen batch norm')
parser.add_argument('--epochs', default=200, type=int, help='The number of training epochs')
parser.add_argument('--batch-size', default=128, type=int, help='The batch size')
parser.add_argument('--learning-rate', default=0.001, type=float, help='The initial learning rate')
parser.add_argument('--decay-rate', default=0.1, type=float, help='The learning rate decay factor')
parser.add_argument('-resume', action='store_true', default=False,
                    help='Whether or not to resume training from a previous run')
parser.add_argument('--prev-log-file', default='', type=str,
                    help='The previous run to resume training from')


def main():
    global best_metrics

    # Parse the arguments
    args = parser.parse_args()

    # Resume from a previous run if asked to
    if args.resume:
        # Read the log file and update the arguments
        args = read_log_file(args)

        # Load the checkpoint
        checkpoint = torch.load('{}/results/{}/checkpoint.pth.tar'.format(HOME_DIR, args.run_id))

        # Determine the start epoch and best metrics
        start_epoch = checkpoint['epoch'] + 1
        best_metrics = (checkpoint['best_acc'], checkpoint['best_pre'], checkpoint['best_rec'])
    else:
        start_epoch = 0
 
    # Set the number of channels, class_lookup, and SummaryWriter
    args.channels = 3 if args.fg_bg_sep else 1
    args.class_lookup = {'AB': 0, 'HB': 1, 'FW': 2, 'MW': 3}
    args.writer = SummaryWriter('{}/logs/tensorboard/{}'.format(HOME_DIR, args.run_id))

    # Set the RNG seeds
    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        torch.backends.cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. \
                       This will turn on the CUDNN deterministic setting, \
                       which can slow down your training considerably! \
                       You may see unexpected behavior when restarting \
                       from checkpoints.')

    # Print out the training setup
    training_print_statement(args)

    # Create the model
    print(" --> creating model")
    device = torch.device('cuda')
    
    if args.frozen_bn:
        norm_layer = torchvision.ops.misc.FrozenBatchNorm2d
    else:
        norm_layer = None
        
    model = models.__dict__[args.architecture](pretrained=False, channels=args.channels, 
                                               num_classes=len(args.class_lookup.keys()), 
                                               norm_layer=norm_layer)
    model = torch.nn.DataParallel(model).to(device)

    # Create the optimizer, etc.
    criterion = torch.nn.CrossEntropyLoss().to(device)
    optimizer = torch.optim.Adam(model.parameters(), args.learning_rate)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=args.decay_rate, patience=10)
    torch.backends.cudnn.benchmark = True

    # Load the model/optimizer from a checkpoint if resuming
    if args.resume:
        print(" --> loading the model weights")
        model.load_state_dict(checkpoint['state_dict'])
        print(" --> copying the optimizer")
        optimizer.load_state_dict(checkpoint['optimizer'])

    # Create the datasets and loaders
    print(' --> creating the datasets and iterators')
    training_dataset = HDF5Dataset('{}/hdf5'.format(DATA_DIR), return_ambient=True, 
                                   pretraining=True, shuffle=True, seed=1234, channels=args.channels, 
                                   seconds=args.seconds, species_lookup=args.class_lookup)

    training_sampler = BalancedBatchSampler(training_dataset, list(args.class_lookup.values()))
    training_loader = torch.utils.data.DataLoader(training_dataset, batch_size=args.batch_size, sampler=training_sampler,
                                                  num_workers=args.num_workers, pin_memory=True)

    # Save the length of the training data loader for Tensorboard
    args.train_loader_len = len(training_loader)

    # Create the validation dataset and loader
    validation_dataset = copy.deepcopy(training_dataset)
    validation_dataset.eval()

    validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=args.batch_size, shuffle=True,
                                                    num_workers=args.num_workers, pin_memory=True)

    # Train the model
    print(' --> starting the training\n')
    for epoch in range(start_epoch, args.epochs):
        # Set the current epoch to be used by Tensorboard
        print(' Epoch [{}/{}]'.format(epoch, args.epochs))
        args.current_epoch = epoch

        # Take a training step
        train(training_loader, model, criterion, optimizer, epoch, device, args)

        # Evaluate on validation set and check if it is the current best
        val_loss, metrics = validate(validation_loader, model, criterion, device, args)
        best_metrics, is_best = test_best_metrics(metrics, best_metrics)

        # Take a step using the learning rate scheduler
        lr_scheduler.step(val_loss)

        # Save the checkpoint
        save_checkpoint({
            'epoch': epoch,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict(),
            'best_acc': best_metrics[0],
            'best_pre': best_metrics[1],
            'best_rec': best_metrics[2]
        }, is_best, args)


if __name__ == '__main__':
    main()
