# engine.py
#
# @author: Mark Thomas
# @modified: 2020-05-09

import os
import shutil
import time
import random
import torch
import torchvision

import numpy as np
import models

from sklearn.metrics import accuracy_score, precision_score, recall_score


def create_model(args, ema=False):
    ''' Creates the model to be trained
    
    Args:
        args (argparse.Namespace): the parsed argparse arguments
        ema (boolean): whether this is an EMA model (default is False)
        
    Returns:
        the created model
    '''
    if args.frozen_bn:
        norm_layer = torchvision.ops.misc.FrozenBatchNorm2d
    else:
        norm_layer = None
        
    model = models.__dict__[args.architecture](pretrained=False, channels=args.channels,
                                               num_classes=len(args.species_lookup.keys()),
                                               norm_layer=norm_layer)
    model = torch.nn.DataParallel(model)
    model = model.cuda()

    if ema:
        for param in model.parameters():
            param.detach_()

    return model


def train(labeled_loader, unlabeled_loader, 
          model, optimizer, ema_optimizer, criterion, 
          epoch, use_cuda, args):
    ''' Trains the model for one full epoch
    
    Args:
        labeled_loader (torch.DataLoader): the dataloader for the labeled data
        unlabeled_loader (torch.DataLoader): the dataloader for the unlabeled data
        model (torch.Module): the model to be trained
        optimizer (torch.optim): the model optimizer
        ema_optimizer (torch.optim): the EMA model optimizer
        criterion (torch.criterion): the model criterion
        epoch (int): the current epoch
        use_cuda (boolean): whether or not CUDA is available
        args (argparse.Namespace): the parsed argparse arguments
        
    Returns:
        A triple containing the labeled, unlabeled, and combined losses
    '''
    losses = AverageMeter()
    losses_x = AverageMeter()
    losses_u = AverageMeter()
    ws = AverageMeter()

    # Make the loaders iterable
    labeled_iterable = iter(labeled_loader)
    unlabeled_iterable = iter(unlabeled_loader)
    
    model.train()
    for batch_idx in range(args.n_labeled):
        # Get the next set of features/targets
        inputs_x, targets_x = labeled_iterable.next()
        (inputs_u, inputs_u2), _ = unlabeled_iterable.next()
        
        # Transform the targets to one-hot encodings
        n_targets = len(list(args.species_lookup.keys()))
        targets_x = torch.zeros(args.batch_size, n_targets).scatter_(1, targets_x.view(-1, 1), 1)
        
        # If using cuda, move the features/targets to the GPU
        if use_cuda:
            inputs_x, targets_x = inputs_x.cuda(), targets_x.cuda(non_blocking=True)
            inputs_u = inputs_u.cuda()
            inputs_u2 = inputs_u2.cuda()

        # Compute the guessed labels of the unlabeled samples
        with torch.no_grad():
            outputs_u = model(inputs_u)
            outputs_u2 = model(inputs_u2)
            p = (torch.softmax(outputs_u, dim=1) + torch.softmax(outputs_u2, dim=1)) / 2
            pt = p ** (1 / args.T)
            targets_u = pt / pt.sum(dim=1, keepdim=True)
            targets_u = targets_u.detach()

        # Perform 'mixup'
        all_inputs = torch.cat([inputs_x, inputs_u, inputs_u2], dim=0)
        all_targets = torch.cat([targets_x, targets_u, targets_u], dim=0)

        l = np.random.beta(args.alpha, args.alpha)
        l = max(l, 1 - l)

        idx = torch.randperm(all_inputs.size(0))

        input_a, input_b = all_inputs, all_inputs[idx]
        target_a, target_b = all_targets, all_targets[idx]

        mixed_input = l * input_a + (1 - l) * input_b
        mixed_target = l * target_a + (1 - l) * target_b

        # Interleave labeled and unlabeled samples between batches to get correct batchnorm calculation 
        mixed_input = list(torch.split(mixed_input, args.batch_size))
        mixed_input = interleave(mixed_input, args.batch_size)

        logits = [model(mixed_input[0])]
        for input in mixed_input[1:]:
            logits.append(model(input))

        # Put the interleaved samples back
        logits = interleave(logits, args.batch_size)
        logits_x = logits[0]
        logits_u = torch.cat(logits[1:], dim=0)

        # Compute the criterion
        Lx, Lu, w = criterion(logits_x, mixed_target[:args.batch_size], logits_u, mixed_target[args.batch_size:], 
                              lambda_u=args.lambda_u, epoch=epoch + (batch_idx / args.n_labeled), args=args)
        
        loss = Lx + w * Lu

        # Record loss
        losses.update(loss.item(), inputs_x.size(0))
        losses_x.update(Lx.item(), inputs_x.size(0))
        losses_u.update(Lu.item(), inputs_x.size(0))
        ws.update(w, inputs_x.size(0))

        # Compute the gradient and take an optimization step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        ema_optimizer.step()

        # Print out progress
        if batch_idx % args.print_freq == 0:
            n_step_chars = len(str(args.n_labeled))
            print_str_header = '| step [{{: >{}}} / {{}}]'.format(n_step_chars)
            print_str = '{} | Loss: {{:.5f}} | Loss_x: {{:.5f}} | Loss_u: {{:.5f}} | W: {{:.5f}}'.format(print_str_header)
            print(print_str.format(batch_idx + 1, args.n_labeled, losses.avg, losses_x.avg, losses_u.avg, ws.avg))

    return losses.avg, losses_x.avg, losses_u.avg


def validate(loader, model, criterion, epoch, use_cuda, args):
    ''' Evaluates the model
    
    Args:
        loader (torch.DataLoader): the dataloader to be evaluated
        model (torch.Module): the model to be trained
        criterion (torch.criterion): the model criterion
        epoch (int): the current epoch
        use_cuda (boolean): whether or not CUDA is available
        args (argparse.Namespace): the parsed argparse arguments
        
    Returns:
        The loss value and evaluation metrics
    '''       
    losses = AverageMeter()
    accuracy = AverageMeter()
    precision = AverageMeter()
    recall = AverageMeter()

    model.eval()
    with torch.no_grad():
        for batch_idx, (inputs, targets) in enumerate(loader):
            # Move the features/targets to the GPU is CUDA is available
            if use_cuda:
                inputs, targets = inputs.cuda(), targets.cuda(non_blocking=True)
            
            # Measure and record the loss
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            losses.update(loss.item(), args.batch_size)
            
            # Measure the evaluation metrics
            acc, pre, rec = get_metrics(outputs, targets)
            accuracy.update(acc, args.batch_size)
            precision.update(pre, args.batch_size)
            recall.update(rec, args.batch_size)

    return losses.avg, accuracy.avg, precision.avg, recall.avg


def get_metrics(output, target):
    ''' Calculates the metrics desired during training and validation

    Args:
        output (Tensor): the model outputs (i.e., predictions)
        target (Tensor): the target outputs (i.e., ground truth)

    Returns:
        a triple containing the accuracy, precision, and recall
    '''
    with torch.no_grad():
        # Cast the tensors to numpy arrays
        output = output.cpu().numpy()
        target = target.cpu().numpy()

        # Get the predictions from logits
        pred = np.argmax(output, 1)

        # Compute the accuracy, precision, and recall
        acc = accuracy_score(target, pred)
        pre = precision_score(target, pred, average='macro')
        rec = recall_score(target, pred, average='macro')

        return acc, pre, rec

                  
class SemiLoss(object):
    def __call__(self, outputs_x, targets_x, outputs_u, targets_u, lambda_u, epoch, args):
        probs_u = torch.softmax(outputs_u, dim=1)

        Lx = -torch.mean(torch.sum(torch.nn.functional.log_softmax(outputs_x, dim=1) * targets_x, dim=1))
        Lu = torch.mean((probs_u - targets_u)**2)

        return Lx, Lu, lambda_u * linear_rampup(epoch, args.epochs)

    
class WeightEMA(object):
    def __init__(self, model, ema_model, lr, alpha=0.999):
        self.model = model
        self.ema_model = ema_model
        self.alpha = alpha
        self.params = list(model.state_dict().values())
        self.ema_params = list(ema_model.state_dict().values())
        self.wd = 0.02 * lr

        for param, ema_param in zip(self.params, self.ema_params):
            param.data.copy_(ema_param.data)

    def step(self):
        one_minus_alpha = 1.0 - self.alpha
        for param, ema_param in zip(self.params, self.ema_params):
            if ema_param.dtype == torch.float32:
                ema_param.mul_(self.alpha)
                ema_param.add_(param * one_minus_alpha)
                # customized weight decay
                param.mul_(1 - self.wd)

def save_checkpoint(state, is_best, checkpoint, filename='checkpoint.pth.tar'):
    
    if not os.path.exists(checkpoint):
        os.mkdir(checkpoint)

    filepath = os.path.join(checkpoint, filename)
    torch.save(state, filepath)
    if is_best:
        shutil.copyfile(filepath, os.path.join(checkpoint, 'model_best.pth.tar'))

        
def linear_rampup(current, rampup_length):
    if rampup_length == 0:
        return 1.0
    else:
        current = np.clip(current / rampup_length, 0.0, 1.0)
        return float(current)

                  
class AverageMeter(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count
                  
            
def interleave_offsets(batch, nu):
    groups = [batch // (nu + 1)] * (nu + 1)
    for x in range(batch - sum(groups)):
        groups[-x - 1] += 1
    offsets = [0]
    for g in groups:
        offsets.append(offsets[-1] + g)
    assert offsets[-1] == batch
    return offsets


def interleave(xy, batch):
    nu = len(xy) - 1
    offsets = interleave_offsets(batch, nu)
    xy = [[v[offsets[p]:offsets[p + 1]] for p in range(nu + 1)] for v in xy]
    for i in range(1, nu + 1):
        xy[0][i], xy[i][i] = xy[i][i], xy[0][i]
    return [torch.cat(v, dim=0) for v in xy]

                  
def training_print_statement(args):
    print("--------------------------------------------------------------------")
    print("                                                                    ")
    print("           PRE-TRAINING SSL BACKBONE FOR MAMMAL-NET v3              ")
    print("                                                                    ")
    print("                       .-'               .--------.                 ")
    print("                  '--./ /     _.---.     | Howdy! |                 ")
    print("                  '-,  (__..-`       \   '--------'                 ")
    print("                     \          .     | /                           ")
    print("                      `,.__.   ,__.--/                              ")
    print("                        '._/_.'___.-`                               ")
    print("                                                                    ")
    print("                                                                    ")
    print("   New training run...\n")
    print("      Torch:             {}".format(torch.__version__))
    print("      Torchvision:       {}".format(torchvision.__version__))
    print("      Run ID:            {}".format(args.run_id))
    print("      Architecture:      {}".format(args.architecture))
    print('      Frozen BatchNorm:  {}'.format(args.frozen_bn))
    print("      Channels:          {}".format(args.channels))             
    print("      Seconds:           {}".format(args.seconds))
    print("      Batch size:        {}".format(args.batch_size))
    print("      Learning rate:     {}".format(args.learning_rate))
    print("      Decay rate:        {}".format(args.decay_rate))
    print("                                                                    ")
    print("--------------------------------------------------------------------")

