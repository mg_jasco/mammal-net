# misc_functions.py
#
# author: Mark Thomas
# modified: 2020-03-30

import argparse
import random
import warnings
import torch
import torchvision

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.patheffects as path_effects
import matplotlib.animation as animation

import models
from models.mask_rcnn import maskrcnn_cnn_fpn
from utils.data_utils import SpeciesDataset

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


class Arguments():
    ''' Sets up the arguments to be used in training
    '''
    def __init__(self, seconds=30, fmax=1024, fg_bg_sep=True, mel=True, channels=3, 
                 split=1, split_type='partially_annotated/random_sample',
                 classes=5, backbone_classes=5, seed=None, print_freq=100,
                 num_workers=40, pretrained_backbone=False, backbone_log_file = '',
                 epochs=100, batch_size=128, learning_rate=0.001, decay_rate=0.1):
        self.seconds = seconds
        self.fmax = fmax
        self.fg_bg_sep = fg_bg_sep
        self.mel = mel
        self.channels = channels
        self.split = split
        self.split_type = split_type
        self.classes = classes
        self.backbone_classes = backbone_classes 
        self.seed = seed
        self.print_freq = print_freq
        self.num_workers = num_workers
        self.pretrained_backbone = pretrained_backbone
        self.backbone_log_file = backbone_log_file
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.decay_rate = decay_rate


class RCNN():
    ''' Loads the trained RCNN model
    
    Args:
        args (Argparse): the argparse namespace containing the arguments needed
                         for loading the model
        run_id (str): the dictionary containing the loaded model checkpoint
        model_best (bool): whether to load the best model or the latest model
    '''
    def __init__(self, args, run_id, model_best=True):
        self.checkpoint = self.__load_checkpoint(run_id, model_best)
        self.model = self.__load_pretrained_model(args)

    def __load_checkpoint(self, run_id, model_best):
        ''' Loads the model checkpoint
        '''
        if model_best:
            checkpoint_type = 'model_best'
        else:
            checkpoint_type = 'checkpoint'
        
        checkpoint = torch.load('{}/results/{}/{}.pth.tar'.format(HOME_DIR, run_id, checkpoint_type))
        
        return checkpoint
        
    def __load_pretrained_model(self, args):
        ''' Loads the pretrained ResNet-50 model
        '''
        device = torch.device('cuda')
        model = maskrcnn_cnn_fpn(backbone=args.backbone_architecture, channels=args.channels,
                                 num_classes=args.classes, backbone_classes=args.backbone_classes,
                                 backbone_checkpoint=args.backbone_checkpoint).to(device)
        model.roi_heads.mask_roi_pool = None
        model.load_state_dict(self.checkpoint['state_dict'])

        return model

        
class Backbone():
    ''' Loads the trained model backbone
    
    Args:
        args (Argparse): the argparse namespace containing the arguments needed
                         for loading the model
        run_id (str): the dictionary containing the loaded model checkpoint
        model_best (bool): whether to load the best model or the latest model
    '''
    def __init__(self, args, run_id, model_best=True):
        self.checkpoint = self.__load_checkpoint(run_id, model_best)
        self.model = self.__load_pretrained_model(args)

    def __load_checkpoint(self, run_id, model_best):
        ''' Loads the model checkpoint
        '''
        if model_best:
            checkpoint_type = 'model_best'
        else:
            checkpoint_type = 'checkpoint'
        
        checkpoint = torch.load('{}/results/{}/{}.pth.tar'.format(HOME_DIR, run_id, checkpoint_type))
        
        return checkpoint
        
    def __load_pretrained_model(self, args):
        ''' Loads the pretrained ResNet-50 model
        '''
        device = torch.device('cuda')
        model = models.__dict__[self.args.backbone_architecture](
            pretrained=False, channels=args.channels, num_classes=args.backbone_classes,
            norm_layer=torchvision.ops.misc.FrozenBatchNorm2d
        )
        model = torch.nn.DataParallel(model).to(device)
        model.load_state_dict(self.checkpoint['state_dict'])
        model = model.module
        
        return model


def detach(x):
    if isinstance(x, dict):
        return {k: v.detach().cpu().numpy() for (k, v) in x.items()}
    else:
        return x.detach().cpu().numpy()


def plot_predictions(features, targets, dictionary, model, ax, args,
                     add_targets=True, add_predictions=True, score_thresh=0.8, 
                     linewidth=1.5, fontsize=18):
    ''' Plots the predictions on top of a spectrogram
    '''
    reverse_class_lookup = {v: k for k, v in args.class_lookup.items()}
    
    def add_rect(box, label, score, linewidth, linestyle, colour, ax):
        x0 = box[0] / features.shape[2] * args.seconds
        y0 = box[1] / features.shape[1] * args.fmax
        x1 = box[2] / features.shape[2] * args.seconds
        y1 = box[3] / features.shape[1] * args.fmax
        ax.add_patch(patches.Rectangle((x0, y0), x1 - x0, y1 - y0, 
                                       linewidth=linewidth, linestyle=linestyle, 
                                       edgecolor=colour, facecolor='none'))
        if score is not None:
            txt = ax.text(x1, y1, '{:.3f}'.format(score), color=colour, fontsize=fontsize)
        else:
            txt = ax.text(x0, y1, reverse_class_lookup[label], color=colour, fontsize=fontsize)
        txt.set_path_effects([path_effects.withStroke(linewidth=linewidth, foreground='w')])

    # Get the predictions
    model.eval()
    predictions = detach(model([features])[0])
    
    # Plot the spectrogram
    ax.imshow(features[0, :, :], origin='lower', cmap='Blues', 
              extent=(0, args.seconds, 0, args.fmax), aspect='auto')
            
    # Add the annotations
    if add_targets:
        for i in range(len(targets['boxes'])):
            box = targets['boxes'][i]
            label = int(targets['labels'][i])
            add_rect(box, label, None, linewidth, '--', 'b', ax)
            
    # Add the predictions
    if add_predictions:
        for i in range(len(predictions['boxes'])):
            score = predictions['scores'][i]
            if score > score_thresh:
                box = predictions['boxes'][i]
                label = int(predictions['labels'][i])
                add_rect(box, label, score, linewidth, '--', 'r', ax)
            
    return ax
