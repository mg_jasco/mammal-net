# save_hdf5_data.py
#
# author: Mark Thomas
# modified: 2020-05-22

import os.path
import re
import glob
import h5py
import tqdm
import pickle
import librosa
import soundfile

import numpy as np
import pandas as pd
import multiprocessing as mp

from pathlib import Path

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel

ANNOTATIONS = pd.read_csv('{}/data/metadata/BW_FW_SW_training_data_2020-10-22.csv'.format(HOME_DIR))
FILES = list(set(ANNOTATIONS['audiofile']))
ANNOTATIONS = ANNOTATIONS.to_dict('index')

MAX_FREQ = 1024
START_OFFSET = 10
SECONDS = 60
OVERLAP = 10
SAMPLE_RATE = 8000
N_FFT = 2048


def create_spectrogram(filepath):
    # Load the file using soundfile and re-sample to 8000Hz
    y, sr = soundfile.read(filepath)
    if not sr == SAMPLE_RATE:
        y = librosa.core.resample(np.asfortranarray(y), sr, SAMPLE_RATE)
        
    # Compute the spectrogram
    D, _ = librosa.magphase(librosa.core.stft(y, n_fft=N_FFT))

    # Determine the frequency indices to keep
    f_temp = librosa.fft_frequencies(sr=SAMPLE_RATE, n_fft=N_FFT)
    f_keep = np.where(f_temp <= MAX_FREQ)

    # Create the filter and insure it is no greated than the input
    margin = np.sqrt(15)
    D_filter = np.minimum(D, librosa.decompose.nn_filter(D, aggregate=np.median, metric='cosine',
                                                         width=int(librosa.time_to_frames(np.sqrt(15), sr=SAMPLE_RATE))))

    # Create the masks
    fg_mask = librosa.util.softmask(D_filter, margin * (D - D_filter), power=2)
    bg_mask = librosa.util.softmask(D - D_filter, margin * D_filter, power=2)

    # Strip out the FG/BG
    D_fg = fg_mask * D
    D_bg = bg_mask * D

    # Convert D, D_fg, and D_bg to dB and truncate
    D = librosa.amplitude_to_db(D, ref=np.max)
    D_fg = librosa.amplitude_to_db(D_fg, ref=np.max)
    D_bg = librosa.amplitude_to_db(D_bg, ref=np.max)

    D = D[f_keep, :][0, :, :]
    D_fg = D_fg[f_keep, :][0, :, :]
    D_bg = D_bg[f_keep, :][0, :, :]

    # Concatenate the three arrays on top of one another
    D = np.dstack((D, D_fg, D_bg))
    D = np.rollaxis(D, 2, 0)

    # Rescale the numpy array between 0 and 1
    D = (D - D.min()) / (D.max() - D.min())
    
    return D, len(y)


def save_a_file(file):
    try:
        # Filter the annotations dictionary to online contain the file in the loop
        anns_to_iter = {k: v for k, v in ANNOTATIONS.items() if v['audiofile'] == file}

        # Create the filepath string
        temp = anns_to_iter[list(anns_to_iter.keys())[0]]
        filepath = '{}/{}'.format(DATA_DIR, temp['filepath'])
        
        # Create the directory for the file and the HDF5 filename
        pathname = '{}/hdf5/{}/{}/{}/{}/{}'.format(DATA_DIR, temp['client'], temp['location'], temp['timeframe'], temp['station'], temp['recorder'])
        path = Path(pathname)
        path.mkdir(parents=True, exist_ok=True)
        hf_filename = re.sub('(.*)\\.wav', '\\1.hdf5', file)
        
        if not os.path.isfile('{}/{}'.format(pathname, hf_filename)):
            # Compute the spectrogram and get the length of the input
            features, len_of_y = create_spectrogram(filepath)

            # Create arrays containing the freqs/times for the spectrogram
            FFT_freqs = librosa.core.fft_frequencies(sr=SAMPLE_RATE, n_fft=N_FFT)
            FFT_times = np.arange(START_OFFSET, len_of_y / SAMPLE_RATE, OVERLAP)
            if (FFT_times[-1] + SECONDS) > (len_of_y / SAMPLE_RATE):
                FFT_times = FFT_times[:-1]    

            # Create a dictionary for the parts of the spectrogram to save
            N = len(FFT_times)
            data_to_save = {i: {'start_time': None, 'end_time': None, 'start_frame': None, 'end_frame': None, 'features': None,
                                'targets': {'boxes': [], 'species': [], 'call_classes': [], 'call_types': []}} for i in range(N - 1)}

            # Specify the start and end times/frames and include the corresponding features in the 'data_to_save' dictionary
            for i in range(N - 1):    
                start_time = FFT_times[i]
                end_time = start_time + SECONDS
                frames = librosa.core.time_to_frames([start_time, end_time], sr=SAMPLE_RATE, hop_length=512, n_fft=N_FFT)

                data_to_save[i]['start_time'] = start_time
                data_to_save[i]['end_time'] = end_time
                data_to_save[i]['start_frame'] = frames[0]
                data_to_save[i]['end_frame'] = frames[1]
                data_to_save[i]['features'] = features[:, :, frames[0]:frames[1]]

            # Loop over the annotations and include them in the correct part of 'parts_to_save'
            for k, v in anns_to_iter.items():
                # Get the annotation start/ends times
                ann_start_time = v['starttime_sec']
                ann_end_time = v['endtime_sec']

                # Loop over the 'data_to_save' and add the annotation if it falls within the start/end times
                for i in range(N - 1):
                    segment_start_time = data_to_save[i]['start_time']
                    segment_end_time = data_to_save[i]['end_time']

                    if (not ann_start_time >= segment_end_time) and (not ann_end_time <= segment_start_time) and (v['lowfreq'] < MAX_FREQ):
                        ann_start_time = np.maximum(ann_start_time, segment_start_time) - segment_start_time
                        ann_end_time = np.minimum(ann_end_time, segment_end_time) - segment_start_time
                        ann_frames = librosa.core.time_to_frames([ann_start_time, ann_end_time], sr=SAMPLE_RATE, hop_length=512, n_fft=N_FFT)

                        # Create the bbox times/freqs
                        bbox_start_time = ann_frames[0] - 1
                        bbox_end_time = ann_frames[1] + 1
                        bbox_low_freq = np.argmin(FFT_freqs <= v['lowfreq']) - 1
                        bbox_high_freq = np.argmax(FFT_freqs >= v['highfreq']) + 1

                        # Ensure the bounding box is within the bounds of the features matrix
                        bbox_start_time = np.maximum(bbox_start_time, 0)
                        bbox_end_time = np.minimum(bbox_end_time, data_to_save[i]['features'].shape[2])
                        bbox_low_freq = np.maximum(bbox_low_freq, 0)
                        bbox_high_freq = np.minimum(bbox_high_freq, data_to_save[i]['features'].shape[1])

                        cond_1 = (bbox_end_time - bbox_start_time > 0)
                        cond_2 = (bbox_high_freq - bbox_low_freq > 0)
                        cond_3 = (bbox_start_time + 5 < data_to_save[i]['features'].shape[2])
                        if cond_1 and cond_2 and cond_3:
                            data_to_save[i]['targets']['boxes'].append([bbox_start_time, bbox_low_freq, bbox_end_time, bbox_high_freq])
                            data_to_save[i]['targets']['species'].append(v['species'])
                            data_to_save[i]['targets']['call_classes'].append(v['callclass'])
                            data_to_save[i]['targets']['call_types'].append(v['calltype'])

            # Save the data using HDF5
            hf = h5py.File('{}/{}'.format(pathname, hf_filename), 'w')

            for k, v in data_to_save.items():
                g = hf.create_group('{}'.format(k))
                g.create_dataset('features', data=v['features'])
                g.create_dataset('start_time', data=v['start_time'])
                g.create_dataset('end_time', data=v['end_time'])
                g.create_dataset('start_frame', data=v['start_frame'])
                g.create_dataset('end_frame', data=v['end_frame'])

                if len(v['targets']['boxes']) > 0:
                    g.create_dataset('boxes', data=v['targets']['boxes'])
                    g.create_dataset('species', data=np.string_(v['targets']['species']))
                    g.create_dataset('call_classes', data=np.string_(v['targets']['call_classes']))

            hf.close()
            
        return None
    except Exception as e:
        print(e)
        return (file, e)

    
def main():
    bad_files = []
    exceptions = []
    
    with mp.Pool(processes=mp.cpu_count()) as pool:
        with tqdm.tqdm(total=len(FILES), desc='fails = {} | '.format(len(bad_files))) as pbar:
            for i, r in enumerate(pool.imap_unordered(save_a_file, FILES)):
                if r is not None:
                    bad_files.append(r[0])
                    exceptions.append(r[1])
                    pbar.set_description('fails = {} | '.format(len(bad_files)))
                pbar.update()
                
    with open('{}/data/bad_files.pickle'.format(HOME_DIR), 'wb') as f:
        pickle.dump(bad_files, f)
                
    with open('{}/data/exceptions.pickle'.format(HOME_DIR), 'wb') as f:
        pickle.dump(exceptions, f)       


if __name__ == '__main__':
    main()
