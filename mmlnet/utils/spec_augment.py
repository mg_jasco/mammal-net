# spec_augment.py
#
# @author: Mark Thomas
# @modified: 2020-05-09

import copy
import random
import torch
import numpy as np
from utils.sparse_image_warp import time_warp


class SpecAugment():
    ''' Performs the SpecAugment transformation.
    
    Args: 
        time_warping (int): the width of the time warp
        freq_masking (int): the width in frames of the freq masks
        time_masking (int): the width in frames of the time masks
        n_time_masks (int): the number of time masks to include
        n_freq_masks (int): the number of freq masks to include
        
    Returns:
        the augmented spectrogram according to SpecAugment
    '''
    def __init__(self, time_warping=50, freq_masking=50, time_masking=50, n_time_masks=1, n_freq_masks=1):
        self.time_warping = time_warping
        self.freq_masking = freq_masking
        self.time_masking = time_masking
        self.n_time_masks = n_time_masks
        self.n_freq_masks = n_freq_masks
        
    def __call__(self, spectrogram):
        channels = spectrogram.shape[0]
        height = spectrogram.shape[1]
        width = spectrogram.shape[2]
        
        # Time warping
        if channels == 1 and self.time_warping > 0:
            spectrogram = time_warp(spectrogram.squeeze(), self.time_warping)
            spectrogram = spectrogram.unsqueeze(0)
            
        # Frequency masking
        for i in range(self.n_freq_masks):
            f = int(np.random.uniform(0, self.freq_masking))
            f0 = random.randint(0, height - f)
            spectrogram[:, f0:(f0 + f), :] = 0

        # Time masking
        for i in range(self.n_time_masks):
            t = int(np.random.uniform(0, self.freq_masking))
            t0 = random.randint(0, width - t)
            spectrogram[:, :, t0:(t0 + t)] = 0
    
        return spectrogram
    

class AddGaussianNoise():
    ''' Adds Gaussian noise to the spectrogram
    '''
    def __init__(self, freq=False):
        self.freq = freq
    
    def __call__(self, spectrogram):
        height = spectrogram.shape[1]
        width = spectrogram.shape[2]
        
        if self.freq:
            mu = torch.mean(spectrogram, axis=2)
            sigma = torch.std(spectrogram, axis=2)
            noise = np.repeat(np.random.normal(mu, sigma), width, axis=1).reshape(*spectrogram.shape)
        else:
            mu = torch.mean(spectrogram, axis=(1, 2))
            sigma = torch.std(spectrogram, axis=(1, 2))
            noise = [np.random.normal(m, s, height * width).reshape((height, width)) for m, s in zip(mu, sigma)]
        
        noise = torch.as_tensor(np.array(noise), dtype=torch.float32)
        spectrogram = torch.add(spectrogram, noise)
        spectrogram = (spectrogram - spectrogram.min()) / (spectrogram.max() - spectrogram.min())
        
        return spectrogram

    
class TransformTwice:
    def __init__(self, transform):
        self.transform = transform

    def __call__(self, instance):
        instance2 = copy.deepcopy(instance)
        out1 = self.transform(instance)
        out2 = self.transform(instance2)
        return out1, out2