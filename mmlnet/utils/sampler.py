import torch
import random
import multiprocessing as mp
from tqdm import tqdm
from torch.utils.data.sampler import Sampler


class BalancedBatchSampler(Sampler):
    def __init__(self, dataset, classes, bboxes=False):
        self.DATASET = dataset
        self.BBOXES = bboxes
        
        self.N = len(dataset)
        self.dataset = {c: list() for c in classes}
        self.iter_over_dataset()
        
        self.balanced_max = 0
        for c in classes:
            if len(self.dataset[c]) > self.balanced_max:
                self.balanced_max = len(self.dataset[c])
        
        for c in classes:
            while len(self.dataset[c]) < self.balanced_max:
                self.dataset[c].append(random.choice(self.dataset[c]))
        
        self.keys = list(self.dataset.keys())
        self.currentkey = 0
        self.indices = [-1] * len(self.keys)

    def __iter__(self):
        while self.indices[self.currentkey] < self.balanced_max - 1:
            self.indices[self.currentkey] += 1
            yield self.dataset[self.keys[self.currentkey]][self.indices[self.currentkey]]
            self.currentkey = (self.currentkey + 1) % len(self.keys)
        self.indices = [-1]*len(self.keys)
        
    def __len__(self):
        return self.balanced_max * len(self.keys)

    def iter_over_dataset(self):
        with mp.Pool(processes=mp.cpu_count()) as pool:
            with tqdm(total=self.N) as pbar:
                for i, l in enumerate(pool.imap(self.get_a_label, range(self.N))):
                    self.dataset[l].append(i)
                    pbar.update()
    
    def get_a_label(self, idx):
        if self.BBOXES:
            _, target = self.DATASET.__getitem__(idx)
            label = target['labels'][0]
        else:
            _, label = self.DATASET.__getitem__(idx)
        return int(label)
