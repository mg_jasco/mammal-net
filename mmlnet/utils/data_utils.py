# data_utils.py
#
# author: Mark Thomas
# modfied: 2020-03-11

import os
import re
import h5py
import soundfile
import librosa
import torch
import random
import pickle

import numpy as np
import pandas as pd
import multiprocessing as mp

from PIL import Image
from torch.utils.data import Dataset
from glob import glob
from tqdm import tqdm

import warnings
import configparser

warnings.filterwarnings('ignore')
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel

QUERY_TARGETS = False
VALID_SPECIES = None
FILES_AVAILABLE = [re.sub('\\\\', '/', f) for f in glob('{}/**/*.wav'.format(DATA_DIR), recursive=True)]

with open('{}/data/metadata/fully_annotated_files.txt'.format(HOME_DIR), 'r') as file:
    FULLY_ANNOTATED_FILES = [re.sub('(.*)\.wav\n', '{}/hdf5/\\1.hdf5'.format(DATA_DIR), f) for f in file.readlines()]

callclasses = {
    'TC': 'tonal',
    'IS': 'infrasonic',
    'NS': 'non-song',
    'AL': 'all',
    'IF': 'infrasonic',
    'SG': 'song',
    'MM': 'man-made',
    'FF': 'full-frequency',
    'AU': 'audible',
    'A': 'audible',
    'O': 'other',
    'NT': 'non-tonal',
    'RF': 'reduced-frequency',
    'SL': 'song-like',
    'UP': 'upsweep',
    'E': 'echosounder',
    'GS': 'gunshot'
}


class SpeciesDataset(Dataset):
    ''' Creates an interable dataset for a given species and call class when desired.

    Attributes:
        species (str): the species requested (default is None)
        callclass (str): the callclass requested (default is None)
        dataset (str): the dataset type to use (default is the training set)
        split (int): the dataset split to use (default is 1)
        split_type (str): the type of data split to use (default is partially_annotated/random_sample)
        seconds (int): the number of seconds for each spectrogram (default is 15)
        fmax (int): the maxiumum frequency of the spectrograms (default is 1024Hz)
        sr (int): the sampling rate to use (default is 8000Hz)
        nfft (int): the number of samples in an FFT to use (default is 2048)
        mel (bool): whether to convert to Mel-spectrograms (default is True)
        fg_bg_sep (bool): whether to separate the foreground and background (default is True)
        class_lookup (dict): the species/labels of the examples (default corresponds to all available species)
        to_image (bool): whether to cast the final features as an RGB image (default is False)
        return_dict (bool): whether to include the dictionary entry each time __getitem__ is called
                            (default is False)
        pretraining (bool): whether the data is being used for pre-training (default is False)
        seed (int): the RNG seed to use for sampling (default is None)
    '''
    def __init__(self, species=None, callclass=None, dataset='training',
                 split=1, split_type='partially_annotated/random_sample',
                 seconds=15, fmax=1024, sr=8000, nfft=2048, mel=True, fg_bg_sep=True,
                 class_lookup={'BW': 1, 'FW': 2, 'SW': 3, 'RW': 4, 'HB': 5, 'MW': 6, 'NN': 7},
                 pretraining=False, margin=2, to_image=False, return_dict=False, seed=None):
        self.species = species
        self.callclass = callclass
        self.seconds = seconds
        self.fmax = fmax
        self.sr = sr
        self.nfft = nfft
        self.mel = mel
        self.fg_bg_sep = fg_bg_sep
        self.class_lookup = class_lookup
        self.margin = margin
        self.seed = seed
        self.pretraining = pretraining
        self.to_image = to_image
        self.return_dict = return_dict
        self.filedict, self.identifiers, self.species_identifiers = self.__load_data(dataset, split, split_type)

    def __len__(self):
        return len(self.species_identifiers)

    def __getitem__(self, idx):
        # Load the metadata corresponding to the id and 'idx'
        identifier = self.species_identifiers[idx]

        # Read and resample the .wav file
        ts, start_ind, end_ind = self.__read_wav_and_resample(identifier)
        self.tmp = {'d': self.filedict[identifier], 'start_ind': start_ind, 'end_ind': end_ind}

        # Create the spectrograms and the target bboxes
        features = self.__create_spectrogram_features(ts)
        if self.pretraining:
            species_label = self.class_lookup[self.filedict[identifier]['species']]
            target = torch.tensor(species_label, dtype=torch.int64)
        else:
            target = self.__build_bboxes(identifier, idx, features, start_ind, end_ind)

        # Convert to an image if desired
        if self.to_image:
            features = 255 * features.numpy()
            features = features.astype(np.uint8)
            features = np.moveaxis(features, 0, -1)
            features = Image.fromarray(features)
            features = features.rotate(180)
            features = features.transpose(Image.FLIP_LEFT_RIGHT)

        # Return the dictionary element? -- useful for plotting only (WILL BREAK TRAINING)
        if self.return_dict:
            return features, target, self.filedict[identifier]
        else:
            return features, target

    def __load_data(self, dataset, split, split_type):
        ''' Loads and filters a file dictionary for a given dataset/split.

        Args:
            dataset (str): the dataset to use (training, validation, or test)
            split (int): the data split to use (1 - 19)
            split_type (str): the type of data split to use

        Returns:
            A tuple of the filedictionary and a list of the keys (or identifiers) of the dict
        '''
        # Load the pickle file containing the dictionary
        data_dir = '{}/data/splits/{}'.format(HOME_DIR, split_type)
        if split_type == 'partially_annotated/random_sample':
            with open('{}/{}_split_{}.pickle'.format(data_dir, dataset, split), 'rb') as f:
                filedict = pickle.load(f)
        elif split_type == 'partially_annotated/datetime_based':
            with open('{}/{}_set.pickle'.format(data_dir, dataset), 'rb') as f:
                filedict = pickle.load(f)
        elif split_type.startswith('fully_annotated'):
            with open('{}/{}.pickle'.format(data_dir, args.split_type), 'rb') as f:
                filedict = pickle.load(f)
        else:
            raise ValueError('Invalid input for argument "split_type": {}'.format(args.split_type))
        
        filedict = {k: v for k, v in filedict.items() if v['filepath'] in FILES_AVAILABLE}
        
        # Filter the filedict for species
        if self.species is not None:
            species_filedict = {k: v for (k, v) in filedict.items() if v['species'] in self.species}
        else:
            species_filedict = filedict

        # Filter the filedict on callclass
        if self.callclass is not None:
            species_filedict = {k: v for (k, v) in species_filedict.items() if (v['callclass'] in self.callclass) or (v['callclass_desc'] in self.callclass)}

        return filedict, list(filedict.keys()), list(species_filedict.keys())

    def __read_wav_and_resample(self, identifier):
        '''Loads a .wav file using librosa and then samples the waveform around a given annotation.

        Args:
            identifier (str): the identifier containing all the relevant info for the file to be loaded

        Returns:
            the resampled time series (i.e., waveform) and the sampling rate
        '''
        # Determine the file name
        metadata = self.filedict[identifier]
        filename = metadata['filepath']

        # Read the .wav file and resample it to match 'sr'
        ts, old_sr = soundfile.read(filename)
        ts = librosa.core.resample(np.asfortranarray(ts), old_sr, self.sr)

        # Determine the start/end time of the annotation
        start = np.floor(metadata['starttime_sec'])
        end = np.ceil(metadata['endtime_sec'])
        duration = end - start

        # Find the sample indices
        if duration < self.seconds:
            under_duration = self.seconds - duration
            start_ind = random.Random(self.seed).randint(start - under_duration, start)
        else:
            over_duration = duration - self.seconds
            start_ind = random.Random(self.seed).randint(start, start + over_duration)

        # Make sure the start_ind falls within a proper range
        start_ind = np.maximum(0, start_ind)
        start_ind = np.minimum(int(len(ts) / self.sr) - self.seconds, start_ind)

        # The end index is always the start + self.seconds
        end_ind = start_ind + self.seconds

        # Truncate the time series of the waveform
        ts = ts[(start_ind*self.sr):(end_ind*self.sr)]

        return ts, start_ind, end_ind

    def __create_spectrogram_features(self, data):
        ''' Generates a spectrogram using librosa.

        Args:
            data (np.array): the time series data of the waveform

        Returns:
            a numpy array containing the STFT matrix
        '''
        # Generate the spectrogram and remove the phase information
        D, _ = librosa.magphase(librosa.core.stft(data, n_fft=self.nfft))

        # Determine the frequency indices to keep
        f_temp = librosa.fft_frequencies(sr=self.sr, n_fft=self.nfft)
        f_keep = np.where(f_temp <= self.fmax)

        # If separating the FG and BG, the features are a 3 channel tensor
        if self.fg_bg_sep:
            # Create the filter and insure it is no greated than the input
            D_filter = np.minimum(D, librosa.decompose.nn_filter(D, aggregate=np.median, metric='cosine',
                                                                 width=int(librosa.time_to_frames(self.margin, sr=self.sr))))

            # Create the masks
            margin = self.margin
            fg_mask = librosa.util.softmask(D_filter, margin * (D - D_filter), power=2)
            bg_mask = librosa.util.softmask(D - D_filter, margin * D_filter, power=2)

            # Strip out the FG/BG
            D_fg = fg_mask * D
            D_bg = bg_mask * D

            # Convert D, D_fg, and D_bg to dB and truncate
            D = librosa.amplitude_to_db(D, ref=np.max)
            D_fg = librosa.amplitude_to_db(D_fg, ref=np.max)
            D_bg = librosa.amplitude_to_db(D_bg, ref=np.max)

            D = D[f_keep, :][0, :, :]
            D_fg = D_fg[f_keep, :][0, :, :]
            D_bg = D_bg[f_keep, :][0, :, :]

            # Compute a Mel-spectrogram
            if self.mel:
                D = librosa.feature.melspectrogram(S=D, sr=self.fmax * 2)
                D_fg = librosa.feature.melspectrogram(S=D_fg, sr=self.fmax * 2)
                D_bg = librosa.feature.melspectrogram(S=D_bg, sr=self.fmax * 2)

            # Concatenate the three arrays on top of one another
            D = np.dstack((D, D_fg, D_bg))
            D = np.rollaxis(D, 2, 0)
        else:
            # Convert to decibels (dB), truncate using f_keep, and reshape to have channels
            D = librosa.amplitude_to_db(D, ref=np.max)
            D = D[f_keep, :][0, :, :]

            # Compute a Mel-spectrogram
            if self.mel:
                D = librosa.feature.melspectrogram(S=D, sr=self.fmax * 2)

            D = np.reshape(D, (1, *D.shape))

        # Rescale the numpy array between 0 and 1
        features = (D - D.min()) / (D.max() - D.min())
        features = torch.as_tensor(features, dtype=torch.float32)

        return features

    def __build_bboxes(self, identifier, idx, features, start_ind, end_ind):
        ''' Builds the bounding box target for a given identifier.

        Args:
            identifier (str): the identifier of the annotation
            idx (int): the index of the requested __getitem__
            features (np.array): the matrix of features (i.e., spectrogram)
            start_ind (int): the start index of the time series
            end_ind (int): the end index of the time series

        Returns:
            a dictionary containing the target information
        '''

        # Get the metadata/label corresponding to the supplied identifier
        metadata = self.filedict[identifier]
        label = self.class_lookup[metadata['species']]

        # If the features have been seperated into FG/BG just use the first channel
        if self.fg_bg_sep:
            features = features[0, :, :]
            features = np.reshape(features, (1, *features.shape))

        # Get the time indices of the first bounding box relative to the feature matrix
        times = librosa.core.times_like(features, sr=self.sr, hop_length=self.nfft // 4, n_fft=self.nfft)
        start_time = np.maximum(start_ind, metadata['starttime_sec']) - start_ind
        end_time = np.minimum(end_ind, metadata['endtime_sec']) - start_ind
        start_time_ind = np.maximum(np.argmin(times <= start_time) - 1, 0)
        end_time_ind = np.minimum(np.argmax(times >= end_time) + 1, features.shape[2])

        # Get the frequency indices of the first bounding box relative to the feature matrix
        if self.mel:
            freqs = librosa.core.mel_frequencies(fmax=self.fmax)
        else:
            freqs = librosa.core.fft_frequencies(sr=self.sr, n_fft=self.nfft)

        low_freq = metadata['lowfreq']
        high_freq = metadata['highfreq']
        low_freq_ind = np.maximum(np.argmin(freqs <= low_freq) - 1, 0)
        high_freq_ind = np.minimum(np.argmax(freqs >= high_freq) + 1, features.shape[1])
        
        # Fix degenerate boxes
        new_start_time_ind = min(start_time_ind, end_time_ind)
        new_end_time_ind = max(start_time_ind, end_time_ind)
        new_low_freq_ind = min(low_freq_ind, high_freq_ind)
        new_high_freq_ind = max(low_freq_ind, high_freq_ind)
            
        # Initialize the first bounding box
        labels = [label]
        boxes = [[new_start_time_ind, new_low_freq_ind, new_end_time_ind, new_high_freq_ind]]

        # Add the overlap identifiers to the bounding boxes/labels
        overlap_ids = metadata['overlap_identifiers'].split(', ')
        if overlap_ids[0]:
            # If there are more than 5 just take a sample of only 5 of them
            if len(overlap_ids) > 5:
                overlap_ids = random.Random(self.seed).sample(overlap_ids, 5)

            # Loop through the overlap identifiers and add those that are still within frame
            for id in overlap_ids:
                # Get the metadata/label corresponding to the overlapped identifier
                tmp_metadata = self.filedict[id]
                tmp_species = tmp_metadata['species'] 
                
                # Check if this is overlap is a species of interest
                if tmp_species in self.species:    
                    tmp_label = self.class_lookup[tmp_species]
                    
                    # Determine the start/end times for the overlapped identifier
                    tmp_start_time = tmp_metadata['starttime_sec']
                    tmp_end_time = tmp_metadata['endtime_sec']

                    # Make a bunch of conditions that need to be satisfied for this bounding box to be included
                    cond_1 = (tmp_start_time <= end_ind) and (tmp_end_time <= end_ind) and (tmp_start_time >= start_ind) and (tmp_end_time >= start_ind)
                    cond_2 = (tmp_start_time <= end_ind) and (tmp_end_time <= end_ind) and (tmp_start_time <= start_ind) and (tmp_end_time >= start_ind)
                    cond_3 = (tmp_start_time <= end_ind) and (tmp_end_time >= end_ind) and (tmp_start_time >= start_ind) and (tmp_end_time >= start_ind)
                    cond_4 = (tmp_start_time <= end_ind) and (tmp_end_time >= end_ind) and (tmp_start_time <= start_ind) and (tmp_end_time >= start_ind)

                    # If one of the conditions pass then add the bounding box
                    if cond_1 or cond_2 or cond_3 or cond_4:
                        # Truncate the start/end times to be within the visible part of the time series
                        tmp_start_time = np.maximum(tmp_start_time, start_ind) - start_ind
                        tmp_end_time = np.minimum(tmp_end_time, end_ind) - start_ind

                        # Get the time indices of the first bounding box relative to the feature matrix
                        tmp_start_time_ind = np.maximum(np.argmin(times <= tmp_start_time) - 1, 0)
                        tmp_end_time_ind = np.minimum(np.argmax(times >= tmp_end_time) + 1, features.shape[2])

                        # Get the frequency indices of the overlapped bounding box relative to the feature matrix
                        tmp_low_freq = tmp_metadata['lowfreq']
                        tmp_high_freq = tmp_metadata['highfreq']
                        tmp_low_freq_ind = np.maximum(np.argmin(freqs <= tmp_low_freq) - 1, 0)
                        tmp_high_freq_ind = np.minimum(np.argmax(freqs >= tmp_high_freq) + 1, features.shape[1])

                        # Fix degenerate boxes
                        new_start_time_ind = min(tmp_start_time_ind, tmp_end_time_ind)
                        new_end_time_ind = max(tmp_start_time_ind, tmp_end_time_ind)
                        new_low_freq_ind = min(tmp_low_freq_ind, tmp_high_freq_ind)
                        new_high_freq_ind = max(tmp_low_freq_ind, tmp_high_freq_ind)
                        
                        # Make the bounding box for this overlapped identifier
                        tmp_box = [new_start_time_ind, new_low_freq_ind, new_end_time_ind, new_high_freq_ind]

                        # Append the box and label
                        boxes.append(tmp_box)
                        labels.append(tmp_label)

        # Convert the arrays to Tensors and compute the areas
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        areas = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])

        # Suppose all instances are not crowd
        iscrowd = torch.zeros((len(boxes), ), dtype=torch.int64)

        # Make the target dict and return
        target = {}
        target['boxes'] = boxes
        target['labels'] = labels
        target['id'] = torch.tensor([idx])
        target['area'] = areas
        target['iscrowd'] = iscrowd

        return target
    

class StreamingDataset(Dataset):
    ''' Creates an interable dataset for a given species and call class when desired.

    Attributes:
        file (str): the name of the file to load
        seconds (int): the number of seconds for each spectrogram (default is 15)
        overlap (int): the number of seconds to use as an overlap (default is 3)
        fmax (int): the maxiumum frequency of the spectrograms (default is 1024Hz)
        sr (int): the sampling rate to use (default is 8000Hz)
        nfft (int): the number of samples in an FFT to use (default is 2048)
        mel (bool): whether to convert to Mel-spectrograms (default is True)
        fg_bg_sep (bool): whether to separate the foreground and background (default is True)
        class_lookup (dict): the species/labels of the examples (default corresponds to all available species)
        to_image (bool): whether to cast the final features as an RGB image (default is False)
        return_dict (bool): whether to include the dictionary entry each time __getitem__ is called
                            (default is False)
        pretraining (bool): whether the data is being used for pre-training (default is False)
        seed (int): the RNG seed to use for sampling (default is None)
    '''
    def __init__(self, file, targets=False, seconds=15, overlap=3, fmax=1024, sr=8000, nfft=2048,
                 class_lookup={'BW': 1, 'FW': 2, 'SW': 3, 'RW': 4, 'HB': 5, 'MW': 6, 'NN': 7},
                 mel=True, fg_bg_sep=True, margin=2, to_image=False, seed=None):
        self.file = file
        self.targets = targets
        self.sr = sr
        self.ts = self.__read_wav_and_resample()
        self.annotations = self.__load_annotations()
        self.seconds = seconds
        self.overlap = overlap
        self.fmax = fmax
        self.nfft = nfft
        self.class_lookup = class_lookup
        self.mel = mel
        self.fg_bg_sep = fg_bg_sep
        self.margin = margin
        self.seed = seed
        self.to_image = to_image

    def __len__(self):
        length_of_ts = len(self.ts) / self.sr
        self.advance = self.seconds - self.overlap
        
        length, counter = (0, 0)
        while length < length_of_ts:
            counter += 1
            length += self.advance
        
        return counter

    def __getitem__(self, idx):
        
        # Get the portion of the time series to use
        start = idx * self.advance
        end = start + self.seconds
        ts = self.ts[start*self.sr:end*self.sr]

        # Create the spectrograms and the target bboxes
        features = self.__create_spectrogram_features(ts)
        if self.targets:
            target = self.__build_bboxes(idx, features, start, end)
        else:
            target = None

        # Convert to an image if desired
        if self.to_image:
            features = 255 * features.numpy()
            features = features.astype(np.uint8)
            features = np.moveaxis(features, 0, -1)
            features = Image.fromarray(features)
            features = features.rotate(180)
            features = features.transpose(Image.FLIP_LEFT_RIGHT)

        return features, target

    def __read_wav_and_resample(self):
        '''Loads a .wav file using librosa and then samples the waveform around a given annotation.
        
        Returns:
            the resampled time series (i.e., waveform)
        '''
        # Read the .wav file and resample it to match 'sr'
        ts, old_sr = soundfile.read(self.file)
        ts = librosa.core.resample(ts, old_sr, self.sr)
        return ts

    def __load_annotations(self):
        ''' Loads the annotations as a pandas DataFrame and filters only the filepaths == self.file
        
        Returns:
            a pd.DataFrame containing the annotations for the given file
        '''
        df = pd.read_csv('{}/data/metadata/annotated_metadata.csv'.format(HOME_DIR))
        df = df.query('filepath=="{}"'.format(self.file))
        return(df)
        
    def __create_spectrogram_features(self, data):
        ''' Generates a spectrogram using librosa.

        Args:
            data (np.array): the time series data of the waveform

        Returns:
            a numpy array containing the STFT matrix
        '''
        # Generate the spectrogram and remove the phase informationexit
        D, _ = librosa.magphase(librosa.core.stft(data, n_fft=self.nfft))

        # Determine the frequency indices to keep
        f_temp = librosa.fft_frequencies(sr=self.sr, n_fft=self.nfft)
        f_keep = np.where(f_temp <= self.fmax)

        # If separating the FG and BG, the features are a 3 channel tensor
        if self.fg_bg_sep:
            # Create the filter and insure it is no greated than the input
            D_filter = np.minimum(D, librosa.decompose.nn_filter(D, aggregate=np.median, metric='cosine',
                                                                 width=int(librosa.time_to_frames(self.margin, sr=self.sr))))

            # Create the masks
            margin = self.margin
            fg_mask = librosa.util.softmask(D_filter, margin * (D - D_filter), power=2)
            bg_mask = librosa.util.softmask(D - D_filter, margin * D_filter, power=2)

            # Strip out the FG/BG
            D_fg = fg_mask * D
            D_bg = bg_mask * D

            # Convert D, D_fg, and D_bg to dB and truncate
            D = librosa.amplitude_to_db(D, ref=np.max)
            D_fg = librosa.amplitude_to_db(D_fg, ref=np.max)
            D_bg = librosa.amplitude_to_db(D_bg, ref=np.max)

            D = D[f_keep, :][0, :, :]
            D_fg = D_fg[f_keep, :][0, :, :]
            D_bg = D_bg[f_keep, :][0, :, :]

            # Compute a Mel-spectrogram
            if self.mel:
                D = librosa.feature.melspectrogram(S=D, sr=self.fmax * 2)
                D_fg = librosa.feature.melspectrogram(S=D_fg, sr=self.fmax * 2)
                D_bg = librosa.feature.melspectrogram(S=D_bg, sr=self.fmax * 2)

            # Concatenate the three arrays on top of one another
            D = np.dstack((D, D_fg, D_bg))
            D = np.rollaxis(D, 2, 0)
        else:
            # Convert to decibels (dB), truncate using f_keep, and reshape to have channels
            D = librosa.amplitude_to_db(D, ref=np.max)
            D = D[f_keep, :][0, :, :]

            # Compute a Mel-spectrogram
            if self.mel:
                D = librosa.feature.melspectrogram(S=D, sr=self.fmax * 2)

            D = np.reshape(D, (1, *D.shape))

        # Rescale the numpy array between 0 and 1
        features = (D - D.min()) / (D.max() - D.min())
        features = torch.as_tensor(features, dtype=torch.float32)

        return features
    
    def __build_bboxes(self, idx, features, start_ind, end_ind):
        ''' Builds the bounding box target for a given identifier.

        Args:
            idx (int): the index of the requested __getitem__
            features (np.array): the matrix of features (i.e., spectrogram)
            start_ind (int): the start index of the time series
            end_ind (int): the end index of the time series

        Returns:
            a dictionary containing the target information
        '''
        # If the features have been seperated into FG/BG just use the first channel
        if self.fg_bg_sep:
            features = features[0, :, :]
            features = np.reshape(features, (1, *features.shape))

        # Get the time indices of the first bounding box relative to the feature matrix
        times = librosa.core.times_like(features, sr=self.sr, hop_length=self.nfft // 4, n_fft=self.nfft)
   
        # Get the frequency indices of the first bounding box relative to the feature matrix
        if self.mel:
            freqs = librosa.core.mel_frequencies(fmax=self.fmax)
        else:
            freqs = librosa.core.fft_frequencies(sr=self.sr, n_fft=self.nfft)

        # Filter the annotations DataFrame to only contain annotations that match the set of conditions
        time_filters = [end_ind, end_ind, start_ind, start_ind]
        cond_1 = '(starttime_sec <= {}) and (endtime_sec <= {}) and (starttime_sec >= {}) and (endtime_sec >= {})'.format(*time_filters)
        cond_2 = '(starttime_sec <= {}) and (endtime_sec <= {}) and (starttime_sec <= {}) and (endtime_sec >= {})'.format(*time_filters)
        cond_3 = '(starttime_sec <= {}) and (endtime_sec >= {}) and (starttime_sec >= {}) and (endtime_sec >= {})'.format(*time_filters)
        cond_4 = '(starttime_sec <= {}) and (endtime_sec >= {}) and (starttime_sec <= {}) and (endtime_sec >= {})'.format(*time_filters)
        cond_5 = 'lowfreq < {}'.format(self.fmax)
        annotations = self.annotations.query('({} or {} or {} or {}) and ({})'.format(cond_1, cond_2, cond_3, cond_4, cond_5))
        
        boxes = []
        labels = []
        for i in range(len(annotations)):
            # Get row 'i' of the annotations
            anns_ = annotations.iloc[i]
            species = anns_['species'] 

            # Check if this is a species of interest
            if species in self.class_lookup.keys():    
                # Truncate the start/end times to be within the visible part of the time series
                start_time = np.maximum(anns_['starttime_sec'], start_ind) - start_ind
                end_time = np.minimum(anns_['endtime_sec'], end_ind) - start_ind

                # Get the time indices of the first bounding box relative to the feature matrix
                start_time_ind = np.maximum(np.argmin(times <= start_time) - 1, 0)
                end_time_ind = np.minimum(np.argmax(times >= end_time) + 1, features.shape[2])

                # Get the frequency indices of the overlapped bounding box relative to the feature matrix
                low_freq_ind = np.maximum(np.argmin(freqs <= anns_['lowfreq']) - 1, 0)
                high_freq_ind = np.minimum(np.argmax(freqs >= anns_['highfreq']) + 1, features.shape[1])

                # Fix degenerate boxes
                new_start_time_ind = min(start_time_ind, end_time_ind)
                new_end_time_ind = max(start_time_ind, end_time_ind)
                new_low_freq_ind = min(low_freq_ind, high_freq_ind)
                new_high_freq_ind = max(low_freq_ind, high_freq_ind)

                # Append the bounding box and label
                boxes.append([new_start_time_ind, new_low_freq_ind, new_end_time_ind, new_high_freq_ind])
                labels.append(self.class_lookup[species])

        # Convert the arrays to Tensors and compute the areas
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        if len(boxes) > 0:
            areas = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        else:
            areas = torch.tensor([])
            
        # Suppose all instances are not crowd
        iscrowd = torch.zeros((len(boxes), ), dtype=torch.int64)

        # Make the target dict and return
        target = {}
        target['boxes'] = boxes
        target['labels'] = labels
        target['id'] = torch.tensor([idx])
        target['area'] = areas
        target['iscrowd'] = iscrowd

        return target


class HDF5Dataset(Dataset):
    ''' Represents an abstract HDF5 dataset.
    
    Arguments:
        file_path (str): the path to the folder containing the HDF5 files
        annotation_csv (str): the name of the CSV containing the annotations
        seconds (int): the number of seconds of the desired spectrogram (default is 15)
        channels (int): the number of channels to return (default is 3)
        species_lookup (optional, dict): a dictionary containing the species to load if not 
                                         provided, this will create an unlabeled dataset!
        pretraining (boolean): whether the target that should be returned is just the label 
                               or the entire list of target (default is False)
        return_ambient (boolean): whether ambient examples should be returned or only used during initiation
                                  (default is True)
        recursive (boolean): whether to recursively search for h5 files in subdirectories 
                             (default is True)
        transform (optional, torch.Transform): transforms to to apply to the data
        target_transform (optional, torch.Transform): transforms to to apply to the targets
        shuffle (boolean): whether to shuffle the indicies of the training/validation data (default is False)
        training_frac (float): the percentage of the data to use as training data vs validation (default is 0.9)
        seed (optional, int): specifies the RNG seed
    '''
    def __init__(self, file_path, annotation_csv, seconds=15, channels=3, species_lookup=None, pretraining=False, return_ambient=True,
                 recursive=True, transform=None, target_transform=None,  shuffle=False, training_frac=0.9, seed=None):
        self.file_path = file_path
        if recursive:
            self.files = glob('{}/**/*.hdf5'.format(self.file_path), recursive=True)
        else:
            self.files = glob('{}/*.hdf5'.format(self.file_path))

        self.available_to_use = self.__get_available_files(annotation_csv)
        self.files = list(set([f for f in self.files if f in self.available_to_use]))
        self.files = sorted(self.files)
        assert len(self.files) >= 1, 'No hdf5 files found in path {}'.format(self.file_path)
        
        self.species_lookup = species_lookup
        self.reverse_lookup = {v: k for k, v in species_lookup.items()} if self.species_lookup is not None else None
        assert self.species_lookup is None or 'AB' in self.species_lookup.keys(), 'The species lookup must contain an ambient class "AB"'
        
        self.seconds = seconds
        assert self.seconds <= 60, 'Maximum length of 60s'
        
        self.channels = channels
        assert self.channels in [1, 3], '"channels" must be either 1 or 3'
        
        self.training_frac = training_frac
        assert self.training_frac > 0 and self.training_frac < 1, '"training_frac" must be within (0, 1), exclusive'
        
        self.pretraining = pretraining
        self.transform = transform
        self.target_transform = target_transform
        self.training = True
        self.training_dataset, self.validation_dataset = self.__init_dataset(shuffle, seed)
       
        if not return_ambient:
            self.training_dataset = [t for t in self.training_dataset if not t['ambient']]
            self.validation_dataset = [t for t in self.validation_dataset if not t['ambient']]
        
    def __len__(self):
        if self.training:
            return len(list(self.training_dataset))
        else:
            return len(list(self.validation_dataset))
    
    def __getitem__(self, idx):
        features, target = self.__get_data(idx)
        features, new_start_frame, new_end_frame = self.__sample_features(features, target)

        if self.channels == 1:
            features = features[0, :, :]
            features = torch.reshape(features, (1, *features.shape))
        
        if self.species_lookup is not None:
            target = self.__resample_target(target, features, new_start_frame, new_end_frame)  
        
        if self.transform is not None:
            features = self.transform(features)
            
        if self.target_transform is not None:
            target = self.target_transform(target)
        
        return features, target
    
    def __init_dataset(self, shuffle, seed):
        ''' Initializes the dataset.
        
        Args:
            seed (int): the rng seed
            
        Returns:
            a tuple containing the training and test datasets
        '''
        global QUERY_TARGETS, VALID_SPECIES
        
        path_without_slashes = self.file_path.replace('/', '_')
        string_of_species = '_'.join(sorted(list(self.species_lookup.keys()))) if self.species_lookup is not None else 'unlabeled'
        file_name = '{}/hdf5/cache/{}_{}.pickle'.format(DATA_DIR, path_without_slashes, string_of_species)
        
        # Load the dataset if this exact set-up has already been initialized
        dataset_type = 'labeled' if self.species_lookup is not None else 'unlabeled'
        if os.path.isfile(file_name):
            print('--> {} dataset was already initialized, loaded a saved copy'.format(dataset_type))
            with open(file_name, 'rb') as f:
                dataset = pickle.load(f)
        # Otherwise initialize it...
        else:
            if self.species_lookup is not None:
                QUERY_TARGETS = True 
                VALID_SPECIES = set(list(self.species_lookup.keys()))
                
            dataset = []
            num_files = len(self.files)
            with mp.Pool(processes=mp.cpu_count()) as pool:
                with tqdm(desc='--> initializing the {} dataset'.format(dataset_type), total=num_files) as pbar:
                    for i, temp in enumerate(pool.imap(subprocess, self.files)):
                        dataset = dataset + temp
                        pbar.update()

            with open(file_name, 'wb') as f:
                pickle.dump(dataset, f)
            
        if shuffle:
            random.Random(seed).shuffle(dataset)
        
        training_indices = int(len(dataset) * self.training_frac)
        
        return dataset[:training_indices], dataset[training_indices:]

    def __get_data(self, idx):
        ''' Loads the HDF5 file corresponding to the desired index
        
        Args:
            idx (int): the index of the features/targets to retrieve
            
        Returns:
            a tensor containing the features and a dict of numpy arrays
            containing the target information needed before resampling
        '''
        if self.training:
            instance = self.training_dataset[idx]
        else:
            instance = self.validation_dataset[idx]
            
        hf = h5py.File(instance['file'], 'r')
        
        features = np.array(hf.get('{}/features'.format(instance['key']))).astype(np.float32)
        features = torch.from_numpy(features)
        target = torch.from_numpy(np.array([-1], dtype=np.int64))
        
        if self.species_lookup is not None:
            boxes = []
            labels = []
            if instance['ambient']:
                boxes.append(np.array([0, 0, features.shape[2], features.shape[1]]))
                labels.append(self.species_lookup['AB'])
            else:
                all_species = np.array(hf.get('{}/species'.format(instance['key']))).astype(str)
                all_boxes = np.array(hf.get('{}/boxes'.format(instance['key']))).astype(np.float32)
                for i in range(len(all_species)):
                    if all_species[i] in list(self.species_lookup.keys()):
                        boxes.append(all_boxes[i])
                        labels.append(self.species_lookup[all_species[i]])
            
            target = {}
            target['boxes'] = boxes
            target['labels'] = labels
            target['id'] = [idx]
        
        hf.close()
        
        return features, target
    
    def __get_available_files(self, filename):
        ''' Reads the CSV containing the annotations in order to get a list of the
            available annotations.

        Args:
            annotations (str): the name of the annotations file
        
        Returns: 
            a list containing all the annotated files that are available
        '''
        filepath = '{}/data/metadata/{}'.format(HOME_DIR, filename)
        assert os.stat(filepath), 'The annotations file {} was not found in the metadata directory'
        
        df = pd.read_csv(filepath)
        annotated_filepaths = list(set(df['filepath']))
        available_to_use = [re.sub('(.*).wav', '{}/\\1.hdf5'.format(self.file_path), f) for f in annotated_filepaths]

        return available_to_use

    def __sample_features(self, features, target):
        ''' Samples the features to a desired length

        Args:
            features (torch.Tensor): the 3-d tensor to be sampled
            target (dict): the target dictionary such that at least one target remains after sampling

        Returns:
            A tuple containing the re-sampled tensor and the new starting frame 
            so that the target bounding boxes can also be adjusted.
        '''
        features_frames = features.shape[2]
        desired_frames = librosa.core.time_to_frames(self.seconds, sr=8000, hop_length=512, n_fft=2048) - 10

        if features_frames < desired_frames:
            n_padding = desired_frames - features_frames
            start_padding = random.randint(0, n_padding)
            end_padding = n_padding - start_padding
            features = torch.nn.functional.pad(features, (start_padding, end_padding),
                                               mode='constant', value=0)
            features_frames = features.shape[2]
        
        if (self.species_lookup is None) or (self.reverse_lookup[target['labels'][0]] == 'AB'):
            start_frame = random.randint(0, features_frames - desired_frames)
        else:
            random_target = random.randint(0, len(target['boxes']) - 1)
            start_of_random_target = target['boxes'][random_target][0]
            end_of_random_target = target['boxes'][random_target][2]
            
            if start_of_random_target + desired_frames >= features_frames:
                start_frame = features_frames - desired_frames
            elif end_of_random_target - desired_frames <= 0:
                start_frame = 0
            else:
                lower_bound = np.maximum(end_of_random_target - desired_frames, 0)
                start_frame = random.randint(*sorted([lower_bound, start_of_random_target]))
            
        end_frame = start_frame + desired_frames
        features = features[:, :, start_frame:end_frame]

        return features, start_frame, end_frame

    def __resample_target(self, old_targets, features, new_start_frame, new_end_frame):
        ''' Re-samples (or formats) the input targets based on the newly 
            sample features/start frame.
            
        Args: 
            old_targets (dict): a dict of tensors containing the previous targets before 
                                the features were sampled
            features (torch.Tensor): a tensor containing the spectrogram features
            new_start_frame (int): the new start index of the sampled features tensor
            new_end_frame (int): the new end index of the sampled features tensor
            
        Return:
            a dict of tensors containing the targets that are visible within the sampled
            features
        '''
        new_boxes = []
        new_labels = []
        for i in range(len(old_targets['boxes'])):
            box_i = old_targets['boxes'][i]
            label_i = old_targets['labels'][i]
            
            if self.reverse_lookup[label_i] == 'AB':
                new_boxes.append([0, 0, features.shape[2], features.shape[1]])
                new_labels.append(label_i)
            elif (not box_i[0] >= new_end_frame) and (not box_i[2] <= new_start_frame):
                box_i[0] = np.maximum(box_i[0] - new_start_frame, 0)
                box_i[2] = np.minimum(box_i[2] - new_start_frame, features.shape[2])
                new_boxes.append(box_i)
                new_labels.append(label_i)
        
        if self.pretraining:
            if len(new_labels) > 0:
                target = torch.tensor(new_labels[0], dtype=torch.int64)
            else:
                target = torch.tensor(self.species_lookup['AB'], dtype=torch.int64)
        else:
            new_boxes = np.array(new_boxes)
            new_labels = np.array(new_labels)
            target = {}
            target['boxes'] = torch.from_numpy(new_boxes)
            target['labels'] = torch.tensor(new_labels, dtype=torch.int64)
            target['id'] = torch.tensor(old_targets['id'])
            try:
                target['area'] = torch.from_numpy((new_boxes[:, 3] - new_boxes[:, 1]) * (new_boxes[:, 2] - new_boxes[:, 0]))
            except Exception as e:
                print(old_targets)
                print(new_start_frame)
                print(new_end_frame)
                print('\n\n')
                raise e
            target['iscrowd'] = torch.zeros((len(new_boxes), ), dtype=torch.int64)
        
        return target
    
    def train(self):
        self.training = True
    
    def eval(self):
        self.training = False

    
def subprocess(file):
    global QUERY_TARGETS, VALID_SPECIES, FULLY_ANNOTATED_FILES
    
    temp = []
    try:
        with h5py.File(file, 'r', swmr=True) as hf:
            for key in hf.keys():
                if QUERY_TARGETS == hf.get(key).__contains__('boxes'):
                    species = np.array(hf.get(key).get('species')).astype(str)
                    if (VALID_SPECIES is None) or (len(set(species).intersection(VALID_SPECIES)) > 0):
                        temp.append({'file': str(file), 'key': str(key), 'ambient': False})
                    elif file in FULLY_ANNOTATED_FILES:
                        temp.append({'file': str(file), 'key': str(key), 'ambient': True})
                elif file in FULLY_ANNOTATED_FILES:
                    temp.append({'file': str(file), 'key': str(key), 'ambient': True})
    except Exception as e:
        pass
    
    return temp
