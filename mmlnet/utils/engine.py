import re
import math
import sys
import torch
import torchvision
import numpy as np

from tqdm import tqdm
from utils.eval_detection_coco import eval_detection_coco
import utils.utils as utils

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


def train_one_epoch(model, optimizer, data_loader, device, args):
    model.train()
    metric_logger = utils.MetricLogger(delimiter="  ")
    metric_logger.add_meter('lr', utils.SmoothedValue(window_size=1, fmt='{value:.6f}'))
    header = 'Epoch: [{: >3}/{}]'.format(args.current_epoch, args.epochs)

    lr_scheduler = None
    if args.warmup and args.current_epoch == 0:
        warmup_factor = 1. / 1000
        warmup_iters = min(1000, len(data_loader) - 1)
        lr_scheduler = utils.warmup_lr_scheduler(optimizer, warmup_iters, warmup_factor)

    for i, (images, targets) in enumerate(metric_logger.log_every(data_loader, args.print_freq, header)):
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

        loss_dict = model(images, targets)
        losses = sum(loss for loss in loss_dict.values())

        # reduce losses over all GPUs for logging purposes
        loss_dict_reduced = utils.reduce_dict(loss_dict)
        losses_reduced = sum(loss for loss in loss_dict_reduced.values())
        loss_value = losses_reduced.item()

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value))
            print(loss_dict_reduced)
            sys.exit(1)

        optimizer.zero_grad()
        losses.backward()
        optimizer.step()
        
        if lr_scheduler is not None:
            lr_scheduler.step()

        metric_logger.update(loss=losses_reduced)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])
        
        # Write the metrics to Tensorboard
        if i % args.print_freq == 0:
            detached_total_loss = detach(losses_reduced)
            detached_loss_dict = detach(loss_dict_reduced)
            tboard_step = i + (args.train_loader_len * args.current_epoch)
            args.writer.add_scalar('losses/total_loss/training', detached_total_loss, tboard_step)
            args.writer.add_scalar('losses/loss_classifier/training', detached_loss_dict['loss_classifier'], tboard_step)
            args.writer.add_scalar('losses/loss_box_reg/training', detached_loss_dict['loss_box_reg'], tboard_step)
            args.writer.add_scalar('losses/loss_objectness/training', detached_loss_dict['loss_objectness'], tboard_step)
            args.writer.add_scalar('losses/f1-loss_rpn_box_reg/training', detached_loss_dict['loss_rpn_box_reg'], tboard_step)
        

def _get_iou_types(model):
    iou_types = ["bbox"]
    return iou_types


@torch.no_grad()
def evaluate(model, data_loader, device, args, val_loss=True, use_tqdm=False, log_tensorboard=True):
    # Loop over the features/targets and evaluate
    model.eval()
    all_targets = []
    all_predictions = []
    if use_tqdm:
        with tqdm(total=len(data_loader)) as pbar:
            for features, targets in data_loader:
                targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
                all_targets.append([detach(t) for t in targets])
                predictions = model([f.to(device) for f in features])
                all_predictions.append([detach(p) for p in predictions])
                pbar.update(1)
    else:
        print('\nvalidation set...')
        for features, targets in data_loader:
            targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
            all_targets.append([detach(t) for t in targets])
            predictions = model([f.to(device) for f in features])
            all_predictions.append([detach(p) for p in predictions])

    # Flatten the lists and strip out the boxes, labels, and scores
    all_targets = [item for sublist in all_targets for item in sublist]
    gt_bboxes = [t['boxes'] for t in all_targets]
    gt_labels = [t['labels'] for t in all_targets]

    all_predictions = [item for sublist in all_predictions for item in sublist]
    pred_bboxes = [p['boxes'] for p in all_predictions]
    pred_labels = [p['labels'] for p in all_predictions]
    pred_scores = [p['scores'] for p in all_predictions]

    # Run the COCO evaluation
    coco_eval_results = eval_detection_coco(pred_bboxes, pred_labels, pred_scores, 
                                            gt_bboxes, gt_labels)

    # Compile the average precision/recall scores
    AP = coco_eval_results['ap/iou=0.50/area=all/max_dets=100'].tolist()
    AP[0] = coco_eval_results['map/iou=0.50/area=all/max_dets=100']
    mAP = coco_eval_results['ap/iou=0.50:0.95/area=all/max_dets=100'].tolist()
    mAP[0] = coco_eval_results['map/iou=0.50:0.95/area=all/max_dets=100']
    
    AR = coco_eval_results['ar/iou=0.50/area=all/max_dets=100'].tolist()
    AR[0] = coco_eval_results['mar/iou=0.50/area=all/max_dets=100']
    mAR = coco_eval_results['ar/iou=0.50:0.95/area=all/max_dets=100'].tolist()
    mAR[0] = coco_eval_results['mar/iou=0.50:0.95/area=all/max_dets=100']

    # Print our the results
    classes = ['ALL'] + [k for k in args.class_lookup.keys() if not k == 'AB']
    AP_print_statement = dict(zip(classes, ['{:.5f}'.format(i) for i in AP]))
    mAP_print_statement = dict(zip(classes, ['{:.5f}'.format(i) for i in mAP]))
    print('    AP:  {}'.format(AP_print_statement))
    print('   mAP:  {}'.format(mAP_print_statement))

    AR_print_statement = dict(zip(classes, ['{:.5f}'.format(i) for i in AR]))
    mAR_print_statement = dict(zip(classes, ['{:.5f}'.format(i) for i in mAR]))
    print('    AR:  {}'.format(AR_print_statement))
    print('   mAR:  {}'.format(mAR_print_statement))

    metrics = {'AP': AP, 'AR': AR, 'mAP': mAP, 'mAR': mAR}
    if log_tensorboard:
        for m in metrics.keys():
            for i in range(len(classes)):
                args.writer.add_scalar('metrics/{}/{}'.format(m, classes[i]), metrics[m][i], args.current_epoch)
    
    # Compute the validation loss to be used by the LR scheduler
    if val_loss:
        model.train()
        metric_logger = utils.MetricLogger(delimiter="  ")
        for features, targets in data_loader:
            features = [f.to(device) for f in features]
            targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
            loss_dict = model(features, targets)
            loss_dict_reduced = utils.reduce_dict(loss_dict)
            losses_reduced = sum(loss for loss in loss_dict_reduced.values())
            metric_logger.update(loss=losses_reduced, **loss_dict_reduced)

        avg_loss = metric_logger.meters['loss'].avg
        avg_loss_classifier = metric_logger.meters['loss_classifier'].avg
        avg_loss_box_reg = metric_logger.meters['loss_box_reg'].avg
        avg_loss_objectness = metric_logger.meters['loss_objectness'].avg
        avg_loss_rpn_box_reg = metric_logger.meters['loss_rpn_box_reg'].avg

        # Print out the validation loss
        print('loss: {:.4f}\n'.format(avg_loss))
        
        # Write the metrics to Tensorboard
        if log_tensorboard:
            args.writer.add_scalar('losses/total_loss/validation', avg_loss, args.current_epoch)
            args.writer.add_scalar('losses/loss_classifier/validation', avg_loss_classifier, args.current_epoch)
            args.writer.add_scalar('losses/loss_box_reg/validation', avg_loss_box_reg, args.current_epoch)
            args.writer.add_scalar('losses/loss_objectness/validation', avg_loss_objectness, args.current_epoch)
            args.writer.add_scalar('losses/f1-loss_rpn_box_reg/validation', avg_loss_rpn_box_reg, args.current_epoch)
        
        return avg_loss
    else:
        return all_targets, all_predictions


def training_print_statement(args):
    print("--------------------------------------------------------------------")
    print("                                                                    ")
    print("                      TRAINING MAMMAL-NET v3                        ")
    print("                                                                    ")
    print("                       .-'               .--------.                 ")
    print("                  '--./ /     _.---.     | Howdy! |                 ")
    print("                  '-,  (__..-`       \   '--------'                 ")
    print("                     \          .     | /                           ")
    print("                      `,.__.   ,__.--/                              ")
    print("                        '._/_.'___.-`                               ")
    print("                                                                    ")
    print("                                                                    ")
    if args.resume:
        print("   Resumed training run...\n")
    else:
        print("   New training run...\n")
    print('      Torch:               {}'.format(torch.__version__))
    print('      Torchvision:         {}'.format(torchvision.__version__))
    print('      Run ID:              {}'.format(args.run_id))
    print('      Pretrained backbone: {}'.format(args.pretrained_backbone))
    print('      SSL backbone:        {}'.format(args.ssl_backbone))
    print('      Backbone:            {}'.format(args.backbone_architecture))
    print('      Frozen BatchNorm:    {}'.format(args.frozen_bn))
    print('      Seconds:             {}'.format(args.seconds))
    print('      Max frequency:       {}'.format(args.fmax))
    print('      FG/BG separation:    {}'.format(args.fg_bg_sep))
    print('      Mel-spectrogram:     {}'.format(args.mel))
    print('      Batch size:          {}'.format(args.batch_size))
    print('      Learning rate:       {}'.format(args.learning_rate))
    print('      Decay rate:          {}'.format(args.decay_rate))
    print("                                                                    ")
    print("--------------------------------------------------------------------")


def read_log_file(args):
    """ Reads the arguments/info from a log file and changes the namespace of
        the input arguments.
    
    Args:
        args (parsed argparse): the arguments namespace
        
    Return:
        the changed args object with the parsed arguments from the log file
    """
    # If resuming change the args that only resuming should change
    if args.resume:
        ## read the log file
        with open('{}/logs/{}'.format(HOME_DIR, args.prev_log_file), 'r') as f:
            log_file_text = [re.sub('(.*)\\n', '\\1', l) for l in f.readlines()]
 
        ## strip out the previous run ID from the log file
        regex = re.compile('.*Run ID.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.run_id = re.sub('\\s+Run ID:\\s+([a-z0-9\\_]+)', '\\1', text)
        
        ## specify the architecture of the pretrained backbone
        regex = re.compile('.*Backbone.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.backbone_architecture = re.sub('\\s+Backbone:\\s+([a-z0-9\\_]+)', '\\1', text)

        ## change the data-split argument
        regex = re.compile('.*Data split.*')
        try:
            text = list(filter(regex.search, log_file_text))[0]
            args.split = int(re.sub('\\s+Data split:\\s+([0-9]+)', '\\1', text))
        except IndexError:
            args.split = 1
        
        ## change the split type argument
        regex = re.compile('.*Split type.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.split_type = re.sub('\\s+Split type:\\s+([a-z\\/\\_]+)', '\\1', text)
     
        ## change the batch size argument
        regex = re.compile('.*Batch size.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.batch_size = int(re.sub('\\s+Batch size:\\s+([0-9]+)', '\\1', text))
        
        ## change the learning rate argument
        regex = re.compile('.*Learning rate.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.learning_rate = float(re.sub('\\s+Learning rate:\\s+([0-9\\.]+)', '\\1', text))
        
        ## change the decay rate argument
        regex = re.compile('.*Decay rate.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.decay_rate = float(re.sub('\\s+Decay rate:\\s+([0-9\\.]+)', '\\1', text))

    # If loading a pre-trained backbone make the necessary args for doing so
    elif args.pretrained_backbone:
        ## read the log file
        with open('{}/logs/{}'.format(HOME_DIR, args.backbone_log_file), 'r') as f:
            log_file_text = [re.sub('(.*)\\n', '\\1', l) for l in f.readlines()]
        
        ## specify the architecture of the pretrained backbone
        regex = re.compile('.*Architecture.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.backbone_architecture = re.sub('\\s+Architecture:\\s+([a-z0-9\\_]+)', '\\1', text)
        
        ## use the run ID from pretrained log file as a backbone checkpoint location
        regex = re.compile('.*Run ID.*')
        text = list(filter(regex.search, log_file_text))[0]
        text = re.sub('\\s+Run ID:\\s+([a-z0-9\\_]+)', '\\1', text)
        args.backbone_checkpoint = '{}/results/{}/model_best.pth.tar'.format(HOME_DIR, text)
    
    # Change other argparse arguments...
    ## change the seconds argument
    regex = re.compile('.*Seconds.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.seconds = int(re.sub('\\s+Seconds:\\s+([0-9]+)', '\\1', text))
    
    ## change the frozen batchnorm argument
    regex = re.compile('.*Frozen BatchNorm.*')
    text = list(filter(regex.search, log_file_text))[0]
    text = re.sub('\\s+Frozen BatchNorm:\\s+(True|False)', '\\1', text)
    args.frozen_bn = True if text == 'True' else False
    
    # If the backbone was trained using SSL, the log file is slightly different
    if not args.ssl_backbone:
        ## change the fmax argument
        regex = re.compile('.*Max frequency.*')
        text = list(filter(regex.search, log_file_text))[0]
        args.fmax = int(re.sub('\\s+Max frequency:\\s+([0-9]+)', '\\1', text))

        ## change the FG/BG sep argument
        regex = re.compile('.*FG/BG separation.*')
        text = list(filter(regex.search, log_file_text))[0]
        text = re.sub('\\s+FG/BG separation:\\s+(True|False)', '\\1', text)
        args.fg_bg_sep = True if text == 'True' else False

        ## change the mel-spec argument
        regex = re.compile('.*Mel-spectrogram.*')
        text = list(filter(regex.search, log_file_text))[0]
        text = re.sub('\\s+Mel-spectrogram:\\s+(True|False)', '\\1', text)
        args.mel =  True if text == 'True' else False

    return args 
    
    
def detach(x):
    if isinstance(x, dict):
        return {k: v.detach().cpu().numpy() for (k, v) in x.items()}
    else:
        return x.detach().cpu().numpy()
