# pretrain_utils.py
# Utility functions for the pre-training purposes.
#
# author: Mark Thomas
# modifed: 2019-08-24

import os
import re
import shutil
import time
import torch
import torch.nn.parallel
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision
import numpy as np
from tqdm import tqdm
from sklearn.metrics import accuracy_score, precision_score, recall_score

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


def train(loader, model, criterion, optimizer, epoch, device, args):
    """ Training function of the model.

    Args:
        loader (DataLoader): the dataloader of the training set
        model (Model): the model being trained
        criterion (nn.Loss): the loss function of the training routine
        optimizer (torch.optim): the opimization routine
        epoch (int): the current epoch
        device (torch.device): the device to put the tensors on
        args (Namespace): parsed arguments of the ArgumentParser
    """
    # Create the AverageMeters
    batch_time = AverageMeter()
    data_time = AverageMeter()
    xent = AverageMeter()
    acc = AverageMeter()
    pre = AverageMeter()
    rec = AverageMeter()

    # Switch the model to training mode
    model.train()

    # Loop over the training DataLoader
    end = time.time()
    for i, (features, targets) in enumerate(loader):
        # Measure data loading time
        data_time.update(time.time() - end)

        # Move the data to the device
        features = features.to(device)
        targets = targets.to(device)

        # Compute the model output and loss
        output = model(features)
        loss = criterion(output, targets)

        # Measure the training metrics record loss
        acc_temp, pre_temp, rec_temp = get_metrics(output, targets)

        xent.update(loss.item(), features.size(0))
        acc.update(acc_temp, features.size(0))
        pre.update(pre_temp, features.size(0))
        rec.update(rec_temp, features.size(0))

        # Compute the gradient and do a SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Measure the elapsed time
        batch_time.update(time.time() - end)
        end = time.time()
        
        # Print out the metrics and write them to Tensorboard
        if i % args.print_freq == 0:
            print(' ... step [{}/{}]'.format(i, len(loader)))
            print(' ... batch time: {:.3f} ({:.3f})'.format(batch_time.val, batch_time.avg))
            print(' ... loss {:.3f} ({:.3f}) | accuracy {:.3f} ({:.3f}) | '
                  'precision {:.3f} ({:.3f}) | recall {:.3f} ({:.3f})'.
                  format(xent.val, xent.avg, acc.val, acc.avg, pre.val, pre.avg, rec.val, rec.avg))

            tboard_step = i + (args.train_loader_len * args.current_epoch)
            args.writer.add_scalar('losses/cross-entropy/training', xent.avg, tboard_step)
            args.writer.add_scalar('metrics/accuracy/training', acc.avg, tboard_step)
            args.writer.add_scalar('metrics/precision/training', pre.avg, tboard_step)
            args.writer.add_scalar('metrics/recall/training', rec.avg, tboard_step)
            args.writer.add_scalar('metrics/f1-score/training', (2 * pre.avg * rec.avg) / (pre.avg + rec.avg), tboard_step)

        # Write the distributions to Tensorboard half of the time 
        if i % (2 * args.print_freq) == 0:
            tboard_step = i + (args.train_loader_len * args.current_epoch)
            for name, param in model.named_parameters():
                if 'bn' not in name:
                    args.writer.add_histogram(name, param, tboard_step)

                    
def validate(loader, model, criterion, device, args):
    """ Evaluation function for the validation and/or test set.

    Args:
        loader (DataLoader): the dataloader of the validation set
        model (Model): the model being trained
        criterion (nn.Loss): the loss function of the training routine
        device (torch.device): the device to move the tensors onto
        args (Namespace): parsed arguments of the ArgumentParser

    Returns:
        the three validation metrics: accuracy, precision, and recall
    """
    # Create the AverageMeters
    xent = AverageMeter()
    acc = AverageMeter()
    pre = AverageMeter()
    rec = AverageMeter()

    # Switch the model to evaluation mode
    model.eval()

    # Loop over the validation DataLoader
    with torch.no_grad():
        for i, (features, targets) in enumerate(loader):
            # Move the data to the device
            features = features.to(device)
            targets = targets.to(device)

            # Compute the model output and loss
            output = model(features)
            loss = criterion(output, targets)
            xent.update(loss.item(), features.size(0))
            
            # Measure the training metrics
            acc_i, pre_i, rec_i = get_metrics(output, targets)
            acc.update(acc_i, features.size(0))
            pre.update(pre_i, features.size(0))
            rec.update(rec_i, features.size(0))

        # Write the metrics to Tensorboard
        args.writer.add_scalar('losses/cross-entropy/validation', xent.avg, args.current_epoch)
        args.writer.add_scalar('metrics/accuracy/validation', acc.avg, args.current_epoch)
        args.writer.add_scalar('metrics/precision/validation', pre.avg, args.current_epoch)
        args.writer.add_scalar('metrics/recall/validation', rec.avg, args.current_epoch)
        args.writer.add_scalar('metrics/f1-score/validation', (2 * pre.avg * rec.avg) / (pre.avg + rec.avg), args.current_epoch)

        # Print out the metrics
        print(' ... validation set')
        print(' ... loss {:.3f} | accuracy {:.3f} | precision {:.3f} | recall {:.3f}\n'.
              format(xent.avg, acc.avg, pre.avg, rec.avg))

    return xent.avg, (acc.avg, pre.avg, rec.avg)


def validate_test_set(loader, model):
    """ Evaluation function for the validation and/or test set.

    Args:
        loader (DataLoader): the dataloader of the validation set
        model (Model): the model being trained

    Returns:
        the targets and prediction values
    """
    # Switch the model to evaluation mode
    model.eval()

    # Create empty arrays for saving to
    preds = np.array([], dtype=np.int64)
    targets = np.array([], dtype=np.int64)

    # Loop over the test DataLoader
    with torch.no_grad():
        with tqdm(total=len(loader)) as pbar:
            for _, (feature, target) in enumerate(loader):
                # Compute the model output and prediction
                output = model(feature).cpu().numpy()
                pred = np.argmax(output, 1)
                target = target.cpu().numpy()

                # Append the predictions to the numpy array
                preds = np.append(preds, [pred])
                targets = np.append(targets, [target])

                # Update the progress bar
                pbar.update(1)

    return preds[:-1], targets[:-1]


def save_checkpoint(state, is_best, args, filename='checkpoint.pth.tar'):
    """ Saves the model as the current checkpoint, either in general
        or as the current most best, accordingly.

    Args:
        state (dict): the current state of the model
        is_best (bool): whether this is the new best run
        filename (str): the name of the checkpoint file (default is
            checkpoint.pth.tar)
    """
    model_dir = '{}/results/{}'.format(HOME_DIR, args.run_id)
    if not os.path.exists(model_dir):
        os.mkdir(model_dir)

    with open('{}/{}'.format(model_dir, filename), 'wb') as file:
        torch.save(state, file)
    if is_best:
        shutil.copyfile('{}/{}'.format(model_dir, filename), '{}/model_best.pth.tar'.format(model_dir))


def get_metrics(output, target):
    """ Calculates the metrics desired during training and validation

    Args:
        output (Tensor): the model outputs (i.e., predictions)
        target (Tensor): the target outputs (i.e., ground truth)

    Returns:
        a triple containing the accuracy, precision, and recall
    """
    with torch.no_grad():
        # Cast the tensors to numpy arrays
        output = output.cpu().numpy()
        target = target.cpu().numpy()

        # Get the predictions from logits
        pred = np.argmax(output, 1)

        # Compute the accuracy, precision, and recall
        acc = accuracy_score(target, pred)
        pre = precision_score(target, pred, average='macro')
        rec = recall_score(target, pred, average='macro')

        return acc, pre, rec


def test_best_metrics(metrics, best_metrics):
    """ Determines if a set of metrics are better than the
        current best.

    Args:
        metrics (tuple): a triple containing the output of get_metrics

    Returns:
        the current best metrics and a boolean value if the metrics
        provided are equal to the current best
    """
    best_acc, best_pre, best_rec = best_metrics

    is_best = False
    temp_acc = metrics[0]
    temp_pre = metrics[1]
    temp_rec = metrics[2]

    best_comparison = 0.25 * best_acc + 0.75 * (2 * best_pre * best_rec / (best_pre + best_rec + 1e-8))
    temp_comparison = 0.25 * temp_acc + 0.75 * (2 * temp_pre * temp_rec / (temp_pre + temp_rec + 1e-8))

    if temp_comparison > best_comparison:
        best_acc = temp_acc
        best_pre = temp_pre
        best_rec = temp_rec
        is_best = True

    return (best_acc, best_pre, best_rec), is_best


def read_log_file(args):
    """ Reads the arguments/info from a log file and changes the namespace of
        the input arguments.
    
    Args:
        args (parsed argparse): the arguments namespace
        
    Return:
        the changed args object with the parsed arguments from the log file
    
    ----------------------------------------------------------
    Example arguments in a log file for parsing:
    
        Torch:             1.4.0+cu100
        Torchvision:       0.5.0.dev20200105+cu100
        Run ID:            resnet_backbone_datetime_based_2
        Architecture:      resnet101
        Data split:        2
        Split type:        partially_annotated/datetime_based
        Seconds:           30
        Max frequency:     1024
        FG/BG separation:  True
        Mel-spectrogram:   True
        Batch size:        128
        Learning rate:     0.001
        Decay rate:        0.1
    ---------------------------------------------------------
    """
    # Read the log file text
    with open('{}/logs/{}'.format(HOME_DIR, args.prev_log_file), 'r') as f:
        log_file_text = [re.sub('(.*)\\n', '\\1', l) for l in f.readlines()]

    # Strip out the previous run ID from the log file
    regex = re.compile('.*Run ID.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.run_id = re.sub('\\s+Run ID:\\s+([a-z0-9\\_]+)', '\\1', text)

    # Change other argparse arguments...
    ## change the architecture argument
    regex = re.compile('.*Architecture.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.arch = re.sub('\\s+Architecture:\\s+([a-z0-9]+)', '\\1', text)

    ## change the data-split argument
    regex = re.compile('.*Data split.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.split = int(re.sub('\\s+Data split:\\s+([0-9]+)', '\\1', text))

    ## change the split type argument
    regex = re.compile('.*Split type.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.split_type = re.sub('\\s+Split type:\\s+([a-z\\/\\_]+)', '\\1', text)

    ## change the seconds argument
    regex = re.compile('.*Seconds.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.seconds = int(re.sub('\\s+Seconds:\\s+([0-9]+)', '\\1', text))

    ## change the fmax argument
    regex = re.compile('.*Max frequency.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.fmax = int(re.sub('\\s+Max frequency:\\s+([0-9]+)', '\\1', text))
    
    ## change the FG/BG sep argument
    regex = re.compile('.*FG/BG separation.*')
    text = list(filter(regex.search, log_file_text))[0]
    text = re.sub('\\s+FG/BG separation:\\s+(True|False)', '\\1', text)
    args.fg_bg_sep = True if text == 'True' else False

    ## change the mel-spec argument
    regex = re.compile('.*Mel-spectrogram.*')
    text = list(filter(regex.search, log_file_text))[0]
    text = re.sub('\\s+Mel-spectrogram:\\s+(True|False)', '\\1', text)
    args.mel = True if text == 'True' else False

    ## change the batch size argument
    regex = re.compile('.*Batch size.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.batch_size = int(re.sub('\\s+Batch size:\\s+([0-9]+)', '\\1', text))
    
    ## change the learning rate argument
    regex = re.compile('.*Learning rate.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.learning_rate = float(re.sub('\\s+Learning rate:\\s+([0-9\\.]+)', '\\1', text))
    
    ## change the decay rate argument
    regex = re.compile('.*Decay rate.*')
    text = list(filter(regex.search, log_file_text))[0]
    args.decay_rate = float(re.sub('\\s+Decay rate:\\s+([0-9\\.]+)', '\\1', text))
    
    ## change the frozen batchnorm argument
    regex = re.compile('.*Frozen BatchNorm.*')
    text = list(filter(regex.search, log_file_text))[0]
    text = re.sub('\\s+Frozen BatchNorm:\\s+(True|False)', '\\1', text)
    args.frozen_bn = True if text == 'True' else False
    
    return args


def training_print_statement(args):
    print("--------------------------------------------------------------------")
    print("                                                                    ")
    print("              PRE-TRAINING BACKBONE FOR MAMMAL-NET v3               ")
    print("                                                                    ")
    print("                       .-'               .--------.                 ")
    print("                  '--./ /     _.---.     | Howdy! |                 ")
    print("                  '-,  (__..-`       \   '--------'                 ")
    print("                     \          .     | /                           ")
    print("                      `,.__.   ,__.--/                              ")
    print("                        '._/_.'___.-`                               ")
    print("                                                                    ")
    print("                                                                    ")
    if args.resume:
        print("   Resumed training run...\n")
    else:
        print("   New training run...\n")
    print("      Torch:             {}".format(torch.__version__))
    print("      Torchvision:       {}".format(torchvision.__version__))
    print("      Run ID:            {}".format(args.run_id))
    print("      Architecture:      {}".format(args.architecture))
    print('      Frozen BatchNorm:  {}'.format(args.frozen_bn))
    print("      Data split:        {}".format(args.split))
    print("      Split type:        {}".format(args.split_type))
    print("      Seconds:           {}".format(args.seconds))
    print("      Max frequency:     {}".format(args.fmax))
    print("      FG/BG separation:  {}".format(bool(args.fg_bg_sep)))
    print("      Mel-spectrogram:   {}".format(bool(args.mel)))
    print("      Batch size:        {}".format(args.batch_size))
    print("      Learning rate:     {}".format(args.learning_rate))
    print("      Decay rate:        {}".format(args.decay_rate))
    print("                                                                    ")
    print("--------------------------------------------------------------------")


class AverageMeter():
    """ Computes and stores the average and current value
    """
    def __init__(self):
        self.reset()

    def reset(self):
        """ Resets the varriables to zero
        """
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        """ Updates the variables
        """
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count
