# test_maskrcnn.py
#
# author: Mark Thomas
# modifiec: 2020-10-21

import os
import re
import json
import time
import glob
import shutil
import argparse
import pickle
import random
import itertools

import torch
import torchvision
import numpy as np
import pandas as pd
import utils.utils as utils

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.patheffects as path_effects

from tqdm import tqdm, trange
from datetime import datetime

from utils.data_utils import StreamingDataset
from utils.misc_functions import detach
from utils.eval_detection_coco import eval_detection_coco
from models.mask_rcnn import maskrcnn_cnn_fpn

import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


# Input arguments
parser = argparse.ArgumentParser(description='MammalNet v3 Training')

parser.add_argument('--seconds', default=10, type=int,
                    help='The number of seconds of each spectrogram')
parser.add_argument('--fmax', default=500, type=int,
                    help='The maximum frequency of each spectrogram in Hz')
parser.add_argument('-fg-bg-sep', action='store_true', default=False,
                    help='Whether to seperate the foreground and background')
parser.add_argument('-mel', action='store_true', default=False,
                    help='Whether to convert the spectrogram to a mel-scale')
parser.add_argument('--run-id', type=str,
                    help='The run ID and subsequent directory of the model results')
parser.add_argument('--seed', default=None, type=int, help='The RNG seed')
parser.add_argument('--backbone-architecture', default='resnet101', type=str,
                    help='The name of the architecture for the pretrained backbone')

parser.add_argument('-frozen-bn', action='store_true', default=False,
                    help='Whether to use frozen batch norm')
parser.add_argument('--score-threshold', default=0.5, type=float,
                    help='The score threshold of the detector used during filtering')
parser.add_argument('--iou-threshold', default=0.5, type=float,
                    help='The IOU threshold of the detector used during filtering')

args = parser.parse_args()

args.channels = 3 if args.fg_bg_sep else 1
args.class_lookup = {'BW': 1, 'FW': 2, 'SW': 3}
args.classes = 4
args.backbone_classes = 3
args.backbone_checkpoint = None
args.pretrained_backbone = False


def add_rect(ax, box, label, color): 
    x0 = box[0]
    y0 = box[1]
    x1 = box[2]
    y1 = box[3]
    ax.add_patch(patches.Rectangle((x0, y0), x1 - x0, y1 - y0, linewidth=1, linestyle='--', edgecolor=color, facecolor='none'))
    txt = ax.text(x0, y1, label, color=color, fontsize=12)
    txt.set_path_effects([path_effects.withStroke(linewidth=1, foreground='w')])


def calc_iou(a, b):
    intersection_x0 = max(a[0], b[0])
    intersection_x1 = min(a[2], b[2])
    intersection_y0 = max(a[1], b[1])
    intersection_y1 = min(a[3], b[3])
    intersection = (intersection_x1 - intersection_x0) * (intersection_y1 - intersection_y0)
    area_a = (a[2] - a[0]) * (a[3] - a[1])
    area_b = (b[2] - b[0]) * (b[3] - b[1])
    iou = intersection / (area_a + area_b - intersection)
    return max(0, iou)


def filter_targets(t):
    to_include = []
    for i in range(len(t['boxes'])):
        if not int(t['labels'][i]) == 0:
            to_include.append(i)
    return {'boxes': t['boxes'][to_include], 'labels': t['labels'][to_include]}


def filter_predictions(p):
    p_ = list(enumerate(zip(p['boxes'], p['labels'], p['scores'])))
    to_include = []
    for a, b in itertools.combinations(p_, 2):
        if a[1][1] == b[1][1]:
            iou = calc_iou(a[1][0], b[1][0])
            if iou < args.iou_threshold:
                if a[1][2] > b[1][2] and a[1][2] > args.score_threshold:
                    to_include.append(a)
                elif b[1][2] > args.score_threshold:
                    to_include.append(b)
        else:
            if a[1][2] > args.score_threshold:
                to_include.append(a)
    ids = list(set([i[0] for i in to_include]))
    return {'boxes': p['boxes'][ids], 'labels': p['labels'][ids], 'scores': p['scores'][ids]}


def main():
    # Create the model and move it to the GPU
    model = maskrcnn_cnn_fpn(args)
    model.roi_heads.mask_roi_pool = None
    model.roi_heads.mask_head = None
    model.roi_heads.mask_predictor = None
    device = torch.device('cuda')
    model.to(device);

    # Load the previous best model
    checkpoint = torch.load('{}/results/{}/checkpoint.pth.tar'.format(DATA_DIR, args.run_id), map_location=device)
    print(model.load_state_dict(checkpoint['state_dict']))
    model.eval();

    # Determine the files that need tested
    with open('{}/data/metadata/fully_annotated_files.txt'.format(HOME_DIR), 'r') as f:
        files_to_test = [re.sub('(.*)\n', '\\1', l) for l in f.readlines()]
    files_to_test = ['{}/{}'.format(DATA_DIR, f) for f in files_to_test if re.match('^bio.*', f)]

    # Loop over the files and test them
    all_results = {f: {'targets': [], 'predictions': []} for f in files_to_test}
    with tqdm(total = len(files_to_test)) as pbar:
        for file in files_to_test:
            TP, TN, FP, FN = (0 for _ in range(4))

            if os.path.exists(file):
                dataset = StreamingDataset(file, targets=True, seconds=args.seconds, overlap=3, fmax=args.fmax, sr=8000, nfft=2048,
                                           class_lookup=args.class_lookup, mel=args.mel, fg_bg_sep=args.fg_bg_sep, seed=args.seed)

                for i in range(len(dataset)):
                    features, targets = dataset.__getitem__(i)
                    predictions = model([features.to(device)])
                    
                    all_results[file]['targets'].append(detach(targets))
                    all_results[file]['predictions'].append(detach(predictions[0]))

            pbar.update(1)

    # Save the results
    with open('{}/results/{}/BIO_fully_annotated_results.pickle'.format(DATA_DIR, args.run_id), 'wb') as f:
        pickle.dump(all_results, f)


if __name__ == "__main__":
    main()
