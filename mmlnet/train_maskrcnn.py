# train_maskrcnn.py
# Script for training Mammal-Net v3
#
# author: Mark Thomas
# modifed: 2020-03-31

import re
import copy
import random
import argparse
import warnings
import torch
import torchvision
import numpy as np

from torch.utils.tensorboard import SummaryWriter
from utils.data_utils import SpeciesDataset, HDF5Dataset
from utils.engine import train_one_epoch, evaluate, training_print_statement, read_log_file
from utils.spec_augment import AddGaussianNoise, SpecAugment, TransformTwice
from utils.sampler import BalancedBatchSampler
from models.mask_rcnn import maskrcnn_cnn_fpn

import utils.utils as utils
import configparser
cfg = configparser.ConfigParser()
cfg.read_file(open('.config'))
sel = cfg.get('SELECTION', 'config')
HOME_DIR = cfg.get(sel, 'home_dir')
DATA_DIR = cfg.get(sel, 'data_dir')
del cfg, sel


# Setup the ArgumentParser
parser = argparse.ArgumentParser(description='MammalNet v3 Training')

# Data arguments
parser.add_argument('--seconds', default=30, type=int,
                    help='The number of seconds of each spectrogram')
parser.add_argument('--fmax', default=1024, type=int,
                    help='The maximum frequency of each spectrogram in Hz')
parser.add_argument('-fg-bg-sep', action='store_true', default=False,
                    help='Whether to seperate the foreground and background')
parser.add_argument('-mel', action='store_true', default=False,
                    help='Whether to convert the spectrogram to a mel-scale')
parser.add_argument('--split', type=int, default=1,
                    help='The split index to use')
parser.add_argument('--split-type', type=str, default='partially_annotated/random_sample',
                    help='The type of data split to use in training')

# Training arguments
parser.add_argument('--run-id', type=str,
                    help='The run ID and subsequent directory of the model results')
parser.add_argument('--seed', default=None, type=int, help='The RNG seed')
parser.add_argument('--print-freq', default=1000, type=int,
                    help='How often to print out training results')
parser.add_argument('--num-workers', default=16, type=int,
                    help='The number of cores or threads available to the data processor')

# Backbone arguments
parser.add_argument('--backbone-architecture', default='resnet101', type=str,
                    help='The name of the architecture for the pretrained backbone')
parser.add_argument('-frozen-bn', action='store_true', default=False,
                    help='Whether to use frozen batch norm')
parser.add_argument('-pretrained-backbone', action='store_true', default=False, 
                    help='Whether to use a pretrained backbone')
parser.add_argument('-ssl-backbone', action='store_true', default=False, 
                    help='Whether to use a pretrained backbone')
parser.add_argument('--backbone-log-file', default='', type=str,
                    help='The name of the log file for the pretrained backbone')

# Learning arguments
parser.add_argument('--epochs', default=500, type=int, help='The number of training epochs')
parser.add_argument('--batch-size', default=4, type=int, help='The batch size')
parser.add_argument('--learning-rate', default=0.003, type=float, help='The initial learning rate')
parser.add_argument('-warmup', action='store_true', default=False,
                    help='Whether or not to warmup for an epoch')
parser.add_argument('--decay-rate', default=0.1, type=float, help='The learning rate decay factor')
parser.add_argument('-resume', action='store_true', default=False,
                    help='Whether or not to resume training from a previous run')
parser.add_argument('--prev-run-id', default='', type=str,
                    help='The ID of the previous run to resume training from')
parser.add_argument('--prev-log-file', default='', type=str,
                    help='The log file of the previous run to resume training from')


def main():
    # Parse the arguments
    args = parser.parse_args()
    
    # Resume from a previous run if asked to
    if args.resume:
        if not args.prev_log_file == '':
            args = read_log_file(args)
            checkpoint = torch.load('{}/results/{}/checkpoint.pth.tar'.format(DATA_DIR, args.run_id))
        elif not args.prev_run_id == '':
            checkpoint = torch.load('{}/results/{}/checkpoint.pth.tar'.format(DATA_DIR, args.prev_run_id))
        else:
            raise ValueError('Did not provide a previous log file (--prev-log-file) or a previous run ID (--prev-run-id)')
        start_epoch = checkpoint['epoch'] + 1
    elif args.pretrained_backbone:
        args = read_log_file(args)
        start_epoch = 0
    else:
        start_epoch = 0  

    # Set the RNG seegs
    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        torch.backends.cudnn.deterministic = True
        warnings.warn('You have chosen to seed training. \
                       This will turn on the CUDNN deterministic setting, \
                       which can slow down your training considerably! \
                       You may see unexpected behavior when restarting \
                       from checkpoints.')

    # Set the number of channels and SummaryWriter
    args.channels = 3 if args.fg_bg_sep else 1
    args.writer = SummaryWriter('{}/logs/tensorboard/{}'.format(HOME_DIR, args.run_id))

    # Set OR change the class_lookup and number of classes
    args.class_lookup = {'AB': 0, 'BW': 1, 'FW': 2, 'SW': 3}
    args.call_classes = ['tonal', 'audible', 'full-frequency', 'reduced-frequency']
    args.classes = 4
    args.backbone_classes = 3
    
    # Print out the training setup
    training_print_statement(args)

    if args.pretrained_backbone:
        args.backbone_checkpoint = torch.load(args.backbone_checkpoint)
    else:
        args.backbone_checkpoint = None
    
    # Create the model backbone
    print('--> creating the model')
    device = torch.device('cuda')
    model = maskrcnn_cnn_fpn(args)
    model.roi_heads.mask_roi_pool = None
    model.roi_heads.mask_head = None
    model.roi_heads.mask_predictor = None
    model.to(device)
    
    # Construct an optimizer and learning rate scheduler
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=args.learning_rate, momentum=0.9, weight_decay=1e-4)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=args.decay_rate,
                                                              patience=10, verbose=True)
    torch.backends.cudnn.benchmark = True

    if args.resume:
        print("--> loading the model weights")
        model.load_state_dict(checkpoint['state_dict'])
        print("--> copying the optimizer")
        optimizer.load_state_dict(checkpoint['optimizer'])

    # Create the training dataset and loader
    print('--> creating the datasets and iterators')
    training_dataset = HDF5Dataset('{}/hdf5'.format(DATA_DIR), 'BW_FW_SW_training_data_2020-10-22.csv',
                                   seconds=args.seconds, channels=args.channels, 
                                   species_lookup=args.class_lookup, return_ambient=False, pretraining=False, 
                                   recursive=True, shuffle=True, seed=1234)

    # training_dataset = SpeciesDataset(species=list(args.class_lookup.keys()), callclass=args.call_classes, 
    #                                   class_lookup=args.class_lookup, dataset='training', 
    #                                   split=args.split, split_type=args.split_type,
    #                                   seconds=args.seconds, fmax=args.fmax, 
    #                                   mel=args.mel, fg_bg_sep=args.fg_bg_sep)

    training_loader = torch.utils.data.DataLoader(training_dataset, batch_size=args.batch_size, shuffle=True,
                                                  num_workers=args.num_workers, collate_fn=utils.collate_fn, 
                                                  pin_memory=True)

    # Specify the length of the training data loader for Tensorboard
    args.train_loader_len = len(training_loader)
    
    # Create the validation dataset and loader
    validation_dataset = copy.deepcopy(training_dataset)
    validation_dataset.eval()
    
    # validation_dataset = SpeciesDataset(species=list(args.class_lookup.keys()), callclass=args.call_classes,
    #                                     class_lookup=args.class_lookup, dataset='validation',
    #                                     split=args.split, split_type=args.split_type,
    #                                     seconds=args.seconds, fmax=args.fmax,
    #                                     mel=args.mel, fg_bg_sep=args.fg_bg_sep)

    validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=args.batch_size, shuffle=False,
                                                    num_workers=args.num_workers, collate_fn=utils.collate_fn)

    # Train the Model
    prev_loss = 1e7
    print('--> starting the training\n')
    for epoch in range(start_epoch, args.epochs):
        # Set the current epoch to be used by Tensorboard
        args.current_epoch = epoch
        
        # Train for one epoch
        train_one_epoch(model, optimizer, training_loader, device, args)

        # Evaluate on validation set
        val_loss = evaluate(model, validation_loader, device, args)
        
        # Take a step using the learning rate scheduler
        lr_scheduler.step(val_loss)

        if val_loss < prev_loss:
            is_best = True
            prev_loss = val_loss
        else:
            is_best = False

        utils.save_checkpoint({'epoch': epoch, 'state_dict': model.state_dict(), 
                               'optimizer': optimizer.state_dict()}, is_best, args)


if __name__ == '__main__':
    main()
